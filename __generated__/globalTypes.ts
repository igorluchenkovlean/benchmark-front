/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum UserLanguage {
  en = "en",
  ge = "ge",
}

export interface LoginUserInput {
  email: string;
  password: string;
}

export interface RandomUserParams {
  firstName: string;
  lastName: string;
  userEmail: string;
  companyName: string;
}

export interface RegisterUserParams {
  firstName: string;
  lastName: string;
  password: string;
  language: UserLanguage;
  token: string;
}

export interface ReqChangeStatusCompany {
  userId: number;
  status: string;
}

export interface ReqRegisteredUsersCompanies {
  offset?: number | null;
  limit?: number | null;
  status?: string | null;
}

export interface ReqServices {
  id: number;
  companyGroupServiceId: number;
  value?: number | null;
  label: string;
}

export interface ReqUserAndCompanyData {
  firstName?: string | null;
  lastName?: string | null;
  employeesNumber?: number | null;
  country?: string | null;
  city?: string | null;
  postalCode?: string | null;
  siteName?: string | null;
}

export interface UpdatePassword {
  token: string;
  password: string;
}

export interface addCompany {
  email: string;
  industryName: string;
  companyName: string;
  language: UserLanguage;
}

export interface addCompanyDetails {
  employeesNumber: number;
  country: string;
  city: string;
  postalCode: string;
  siteName: string;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
