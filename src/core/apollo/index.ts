import { HttpLink } from 'apollo-boost';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { withClientState } from 'apollo-link-state';

import Storage from 'library/utilities/storage';
import { resolvers } from 'library/graphql/resolvers';
import { initialState } from './initialState';

const onErrorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors) {
		graphQLErrors.map(({ message, locations, path }) =>
			console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`),
		);
	}

	if (networkError) {
		console.log(`[Network error]: ${networkError}`);
	}
});

const createOmitTypenameLink = () =>
	new ApolloLink((operation, forward) => {
		if (operation.variables) {
			operation.variables = JSON.parse(JSON.stringify(operation.variables), omitTypename);
		}

		return forward(operation);
	});

const omitTypename = (key: string, value: any) => (key === '__typename' ? undefined : value);

const httpLink = new HttpLink({
	uri: `${process.env.REACT_APP_HTTPS_URL}/graphql`,
	credentials: 'include',
});

const authLink = setContext((_, { headers }) => {
	const user = Storage.getItem('user');

	const token = user ? user.token : null;

	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : '',
		},
	};
});

const cache = new InMemoryCache({ addTypename: false });
const stateLink = withClientState({
	cache,
	resolvers,
	defaults: initialState,
});

export const apolloClient = new ApolloClient({
	link: ApolloLink.from([createOmitTypenameLink(), onErrorLink, stateLink, authLink.concat(httpLink)]),
	cache,
});
