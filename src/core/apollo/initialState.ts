export interface IUser {
	siteName: string;
	email: string;
	firstName: string;
	lastName: string;
	id: number;
	company: {
		id: number;
		name: string;
		employeesNumber: number;
		country: string;
		city: string;
		postalCode: string;
		industry: {
			name: string;
		};
	};
	language: {
		code: number;
		name: string;
	};
	role: {
		id: number;
		label: string;
	};
}

export interface IUserState {
	token: string;
	isLoggedIn: boolean;
	isInitialData: boolean;
	user: IUser;
}

export const initialState: IUserState = {
	token: '',
	isLoggedIn: false,
	isInitialData: false,
	user: {
		siteName: '',
		email: '',
		firstName: '',
		lastName: '',
		id: 0,
		company: {
			id: 0,
			name: '',
			employeesNumber: 0,
			country: '',
			city: '',
			postalCode: '',
			industry: {
				name: '',
			},
		},
		language: {
			code: 0,
			name: '',
		},

		role: {
			id: 0,
			label: '',
		},
	},
};
