import React, { lazy, useEffect, useState } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { useMutation, useQuery } from '@apollo/react-hooks';

import {
	UPDATE_USER_AUTH_DATA,
	GET_IS_LOGGED_VALUE,
	GET_IS_INITIAL_DATA_VALUE,
} from 'library/graphql/localData/localData';
import { lazyComponent } from './utils/lazyComponent';
import { checkTokenHandler } from './utils/checkTokenHandler';

const Dashboard = lazy(() => import('pages/Dashboard'));
const Login = lazy(() => import('pages/Auth/Login'));
const Register = lazy(() => import('pages/Auth/Register'));
const RegisterRandom = lazy(() => import('pages/Auth/RegisterRandom'));
const ForgotPassword = lazy(() => import('pages/Auth/ForgotPassword'));
const ResetPassword = lazy(() => import('pages/Auth/ResetPassword'));
const Terms = lazy(() => import('pages/TermsAndConditions'));
const CompanyDetails = lazy(() => import('pages/CompanyDetails'));

export const Routes = () => {
	const { data: isLoggedIn } = useQuery(GET_IS_LOGGED_VALUE);
	const { data: isInitialData } = useQuery(GET_IS_INITIAL_DATA_VALUE);
	const [canShowRoutes, setCanShowRoutes] = useState<boolean>(false);

	const [userMutation] = useMutation(UPDATE_USER_AUTH_DATA);
	useEffect(checkTokenHandler({ setCanShowRoutes, userMutation }), []);

	return canShowRoutes ? (
		!isLoggedIn.isLoggedIn ? (
			<Switch>
				<Route exact path='/terms-and-conditions' component={lazyComponent(Terms)} />
				<Route exact path='/login' component={lazyComponent(Login)} />
				<Route path='/register' render={lazyComponent(Register)} />
				<Route path='/register-random' render={lazyComponent(RegisterRandom)} />
				<Route path='/forgot-password' render={lazyComponent(ForgotPassword)} />
				<Route path='/reset-password' render={lazyComponent(ResetPassword)} />

				<Redirect to='/login' />
			</Switch>
		) : isInitialData.isInitialData ? (
			<Switch>
				<Redirect exact from='/' to='/dashboard' />
				<Route exact path='/dashboard' component={lazyComponent(Dashboard)} />
				<Redirect to='/' />
			</Switch>
		) : (
			<Switch>
				<Redirect exact from='/' to='/company-details' />
				<Route exact path='/company-details' component={lazyComponent(CompanyDetails)} />
				<Redirect to='/company-details' />
			</Switch>
		)
	) : null;
};
