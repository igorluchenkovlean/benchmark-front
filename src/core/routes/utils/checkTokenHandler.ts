import { checkToken } from 'library/utilities/token';
import { GraphqlMutationLocal } from 'library/common/types/graphql';

export const checkTokenHandler = ({
	setCanShowRoutes,

	userMutation,
}: {
	setCanShowRoutes: React.Dispatch<React.SetStateAction<boolean>>;

	userMutation: GraphqlMutationLocal;
}) => () => {
	setCanShowRoutes(true);
	checkToken(userMutation);
};
