import React from 'react';
import cn from 'classnames';

import styles from './button.module.scss';

export type ButtonType = 'common' | 'light' | 'red' | 'dark';

interface IButtonProps {
	styles?: { readonly [key: string]: string };
	children: React.ReactNode;
	onClick: () => void;
	isFullwidth?: boolean;
	type?: ButtonType;
	isDisabled?: boolean;
}

const Button: React.FC<IButtonProps> = ({
	children,
	styles: additionalStyles,
	onClick,
	isFullwidth,
	type,
	isDisabled,
}: IButtonProps) => (
	<button
		className={cn(styles.button, { [styles.buttonFullWidth]: isFullwidth }, additionalStyles, getClassByType(type))}
		type='submit'
		onClick={onClick}
		disabled={isDisabled}
	>
		{children}
	</button>
);

export default Button;

Button.defaultProps = {
	type: 'common',
};

export const getClassByType = (type?: ButtonType) => {
	switch (type) {
		case 'light':
			return styles.buttonLight;
		case 'red':
			return styles.buttonRed;
		case 'dark':
			return styles.buttonDark;
		default:
			return '';
	}
};
