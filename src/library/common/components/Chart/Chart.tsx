import React, { useRef, useEffect, useState } from 'react';
import { Axis, select, axisBottom, axisTop, scaleLinear, axisLeft, scaleBand, ScaleBand, ScaleLinear } from 'd3';

import styles from './chart.module.scss';
import useResizeObserver from './hook/useResizeObserver';
import Checkbox from '../Checkbox';

interface IData {
	name: string;
	valueOne: number;
	valueTwo: number;
}

const Chart: React.FC = () => {
	const [data, setData] = useState<IData[]>([
		{
			name: 'Site A',
			valueOne: 94,
			valueTwo: 0,
		},
		{
			name: 'Site B',
			valueOne: 35,
			valueTwo: 0,
		},
		{
			name: 'Site C',
			valueOne: 25,
			valueTwo: 50,
		},
		{
			name: 'Site D',
			valueOne: 75,
			valueTwo: 10,
		},
		{
			name: 'Site E',
			valueOne: 85,
			valueTwo: 10,
		},
	]);
	const [isMedian, setIsMedian] = useState<boolean>(false);
	const [isValuesVisible, setIsValuesVisible] = useState<boolean>(false);
	const [isOutsGrad, setIsOutsGrad] = useState<boolean>(false);
	const svgRef = useRef<SVGSVGElement>(null);
	const wrapperRef = useRef<HTMLDivElement>(null);
	const dimensions = useResizeObserver(wrapperRef);

	useEffect(() => {
		const svg = select(svgRef.current);
		if (!dimensions) return;

		const labelDraw = () => {
			svg
				.selectAll('.label')
				.data(data)
				.join('text')
				.attr('class', 'label')
				.attr('y', (value: IData) => yScale(value.name)! + yScale.bandwidth() / 2)
				.attr('x', (value: IData) => xScale(value.valueOne) - 30)
				.attr('text-anchor', 'middle')
				.text((value: IData) => `${value.valueOne}%`);
		};

		const onMouseOn = (valueActual: IData, i: number) => {
			svg.select(`.bar:nth-of-type(${i + 1})`).attr('fill', 'rgb(107, 107, 107)');
			svg.selectAll('.label').remove();

			svg
				.append('line')
				.attr('id', 'limit')
				.attr('x1', xScale(valueActual.valueOne))
				.attr('y1', 0)
				.attr('x2', xScale(valueActual.valueOne))
				.attr('y2', dimensions.height)
				.attr('stroke', 'black')
				.attr('stroke-width', 2)
				.style('stroke-dasharray', '3, 3');

			svg
				.selectAll('.label')
				.data(data)
				.join('text')
				.attr('class', 'label')
				.attr('y', (value: IData) => yScale(value.name)! + yScale.bandwidth() / 2)
				.attr('x', (value: IData) => xScale(value.valueOne) - 30)
				.attr('text-anchor', 'middle')
				.text((value: IData, index: number) => {
					const divergence = value.valueOne - valueActual.valueOne;

					return index === i ? `${value.valueOne}%` : divergence > 0 ? `+${divergence}%` : `${divergence}%`;
				})
				.on('mouseenter', (valueActualCur: IData, index: number) => onMouseOn(valueActualCur, index))
				.on('mouseleave', (valueActualCur: IData, index: number) => onMouseOut(valueActualCur, index));
		};

		const onMouseOut = (valueActual: IData, i: number) => {
			svg.select(`.bar:nth-of-type(${i + 1})`).attr('fill', 'rgb(171, 171, 171)');
			svg.selectAll('#limit').remove();
			svg.selectAll('.label').remove();
			if (isValuesVisible) {
				labelDraw();
			} else {
				svg.selectAll('.label').remove();
			}
		};

		const xScale: ScaleLinear<number, number> = scaleLinear()
			.range([0, dimensions.width])
			.domain([0, 100]);

		const xScaleTop: ScaleLinear<number, number> = scaleLinear()
			.range([0, dimensions.width])
			.domain([0, 600]);

		const yScale: ScaleBand<string> = scaleBand()
			.range([0, dimensions.height])
			.domain(data.map(value => value.name))
			.padding(0.2);

		const xAxisBottom = axisBottom(xScale)
			.ticks(10)
			.tickSize(-dimensions.height)
			.tickFormat(d => d + '%')
			.tickPadding(10);
		svg
			.select('.x-axisBottom')
			.style('transform', `translateY(${dimensions.height}px )`)

			.call(xAxisBottom as any);

		const xAxisTop = axisTop(xScaleTop)
			.ticks(data.length)
			.tickSize(0)
			.tickPadding(5);
		svg.select('.x-axisTop').call(xAxisTop as any);

		const yAxis: Axis<string> = axisLeft(yScale)
			.tickSize(0)
			.tickPadding(10);
		svg.select('.y-axis').call(yAxis as any);

		svg
			.selectAll('.bar')
			.data(data)
			.join('rect')
			.attr('class', 'bar')
			.attr('y', (value: IData) => yScale(value.name)!)
			.attr('x', 0)
			.attr('width', (value: IData) => xScale(value.valueOne))
			.style('height', yScale.bandwidth() + 'px')
			.on('mouseenter', (valueActual: IData, i: number) => onMouseOn(valueActual, i))
			.on('mouseleave', (valueActual: IData, i: number) => onMouseOut(valueActual, i))
			.attr('fill', 'rgb(171, 171, 171)');

		if (isValuesVisible) {
			labelDraw();
		} else {
			svg.selectAll('.label').remove();
		}
		if (isOutsGrad) {
			svg
				.selectAll('circle')
				.data(data)
				.join('circle')
				.attr('class', 'circle')
				.attr('r', 5)
				.attr('cy', (value: IData) => yScale(value.name)! + yScale.bandwidth() / 2)
				.attr('cx', (value: IData) => xScale(value.valueTwo))
				.on('mouseenter', (valueActual: IData, i: number) => {
					onMouseOn(valueActual, i);
				})
				.on('mouseleave', (valueActual: IData, i: number) => {
					onMouseOut(valueActual, i);
				})
				.attr('fill', 'black');
		} else {
			svg.selectAll('.circle').remove();
		}

		if (isMedian) {
			svg
				.selectAll('.median')
				.data(data)
				.join('line')
				.attr('class', 'median')
				.attr('x1', xScale(85))
				.attr('y1', 0)
				.attr('x2', xScale(85))
				.attr('y2', dimensions.height)
				.attr('stroke', 'blue')
				.attr('stroke-width', 2);
		} else {
			svg.selectAll('.median').remove();
		}
	}, [data, dimensions, isMedian, isValuesVisible, isOutsGrad]);

	const axiesStyle = {
		fontFamily: 'Nunito Sans, sans-serif',
		fontSize: '14px',
		color: 'rgb(171, 171, 171)',
	};

	return (
		<>
			<div className={styles.wrapper} ref={wrapperRef}>
				<svg className={styles.svgContainer} ref={svgRef}>
					<g className='x-axisTop' style={{ ...axiesStyle }} />
					<g className='x-axisBottom' style={{ ...axiesStyle }} />
					<g className='y-axis' style={{ ...axiesStyle, paddingRight: '100px' }} />
				</svg>
			</div>
			<div className={styles.legend}>
				<div onClick={() => setIsMedian(!isMedian)}>
					<Checkbox label={'Median'} checked={isMedian} />
				</div>
				<div onClick={() => setIsOutsGrad(!isOutsGrad)}>
					<Checkbox label={'Outs. Grad'} checked={isOutsGrad} />
				</div>
				<div onClick={() => setIsValuesVisible(!isValuesVisible)}>
					<Checkbox label={'Values'} checked={isValuesVisible} />
				</div>
			</div>
		</>
	);
};

export default Chart;
