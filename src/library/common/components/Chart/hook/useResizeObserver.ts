import { useEffect, useState, RefObject } from 'react';
import ResizeObserver from 'resize-observer-polyfill';

const useResizeObserver = (ref: RefObject<HTMLDivElement>) => {
	const [dimensions, setDimensions] = useState<DOMRectReadOnly>();

	useEffect(() => {
		const observerTarget = ref.current;
		const resizeObserver = new ResizeObserver(entries => {
			entries.forEach(entry => {
				setDimensions(entry.contentRect);
			});
		});
		if (observerTarget) {
			resizeObserver.observe(observerTarget);
		}

		return () => {
			if (observerTarget) {
				resizeObserver.unobserve(observerTarget);
			}
		};
	}, [ref]);

	return dimensions;
};
export default useResizeObserver;
