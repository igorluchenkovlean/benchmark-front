import React from 'react';
import cn from 'classnames';

import WhiteCheck from 'resources/icons/WhiteCheck.png';

import styles from './checkbox.module.scss';

interface ICheckboxProps {
	label?: string;
	checked?: boolean;
}

const Checkbox = ({ label, checked }: ICheckboxProps) => (
	<div className={cn(styles.checkboxContainer, { [styles.checkboxContainerChecked]: checked })}>
		<img className={styles.tic} src={WhiteCheck} alt='' />
		<label className={styles.label}>{label}</label>
	</div>
);

export default Checkbox;
