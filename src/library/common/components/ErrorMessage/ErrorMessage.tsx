import React from 'react';
import { useTranslation } from 'react-i18next';

import styles from './error.module.scss';

interface IErrors {
	errors: string[];
}

const ErrorMessage = ({ errors }: IErrors) => {
	const { t } = useTranslation();
	const arr = errors.filter(error => error !== '');

	return (
		<div className={styles.error}>
			{arr.length > 1 ? t('Errors.We found errors') : ` ${arr[0] ? '* ' + arr[0] : ''}`}
		</div>
	);
};

export default ErrorMessage;
