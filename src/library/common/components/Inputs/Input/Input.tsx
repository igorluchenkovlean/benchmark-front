import React from 'react';
import cn from 'classnames';

import warning from 'resources/icons/warning.svg';

import styles from './input.module.scss';

interface IInputProps {
	label: string;
	value: string;
	onChange: (value: string) => void;
	type?: string;
	error?: string;
	placeholder?: string;
	rightLabel?: string;
	onClick?: () => void;
	setError?: (value?: string) => void;
	isNumber?: boolean;
	isLight?: boolean;
	isServiceQuestion?: boolean;
}

const Input = ({
	label,
	value,
	onChange,
	type,
	error,
	placeholder,
	rightLabel,
	onClick,
	setError,
	isNumber,
	isLight,
	isServiceQuestion,
}: IInputProps) => {
	const onChangeText = (text: string) => {
		if (setError && error) setError(undefined);

		onChange(text);
	};

	return (
		<div className={styles.inputContainer}>
			<div className={styles.labelContainer}>
				{label ? (
					<label
						className={cn(styles.label, {
							[styles.labelLight]: isLight,
							[styles.labelServiceQuestion]: isServiceQuestion,
						})}
					>
						{label}
					</label>
				) : null}
			</div>

			<div className={styles.inputContainer}>
				<input
					className={cn(styles.input, {
						[styles.inputLight]: isLight,
						[styles.ServiceQuestion]: isServiceQuestion,
					})}
					value={value}
					onChange={
						isNumber
							? ({ target }) => onChangeText(target.value.replace(/\D/, ''))
							: ({ target }) => onChangeText(target.value)
					}
					type={type}
					placeholder={placeholder}
				/>
				{error && <img className={styles.warning} src={warning} alt='' />}
			</div>
			{onClick ? (
				<div className={styles.rightLabel} onClick={onClick}>
					{rightLabel}
				</div>
			) : null}
		</div>
	);
};

export default Input;
