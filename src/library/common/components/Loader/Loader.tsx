import React from 'react';

import styles from './loader.module.scss';

const Loader = () => (
	<div className={styles.container}>
		<div className={styles.item1} />
		<div className={styles.item2} />
		<div className={styles.item3} />
		<div className={styles.item4} />
		<div className={styles.item5} />
	</div>
);

export default Loader;
