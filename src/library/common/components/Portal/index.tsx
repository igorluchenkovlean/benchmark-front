import { ReactNode } from 'react';
import { createPortal } from 'react-dom';

interface IProps {
	children: ReactNode;
	modalRoot: HTMLElement;
}

const Portal = ({ children, modalRoot }: IProps) => createPortal(children, modalRoot);

Portal.defaultProps = {
	modalRoot: document.getElementById('modal-portal') || document.createElement('div'),
};

export default Portal;
