import React from 'react';
import Loader from '../Loader';

interface IWithLoaderProps {
	isLoading: boolean;
	children: React.ReactElement;
}

const WithLoader = ({ isLoading, children }: IWithLoaderProps) => (isLoading ? <Loader /> : children);

export default WithLoader;
