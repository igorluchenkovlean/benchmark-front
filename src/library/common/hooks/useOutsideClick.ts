import React, { useEffect } from 'react';

export const useOutsideClick = (ref: React.RefObject<HTMLDivElement>, closeHandler: () => void) => {
	const handleClickOutside = (event: MouseEvent) => {
		if (ref.current && !ref.current.contains(event.target as any)) {
			closeHandler();
		}
	};
	useEffect(() => {
		document.addEventListener('mousedown', handleClickOutside);

		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	});
};
