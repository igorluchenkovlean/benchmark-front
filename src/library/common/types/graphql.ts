import { MutationFunctionOptions, ExecutionResult } from '@apollo/react-common';

export type GraphqlMutationLocal = (
	options?: MutationFunctionOptions<any, Record<string, any>> | undefined,
) => Promise<ExecutionResult<any>>;

export type GraphqlMutation<TData, TVariables> = (
	options?: MutationFunctionOptions<TData, TVariables> | undefined,
) => Promise<ExecutionResult<TData>>;

export enum UserLanguage {
	en = 'en',
	ge = 'ge',
}
