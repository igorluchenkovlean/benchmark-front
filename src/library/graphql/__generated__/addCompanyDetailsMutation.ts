/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { addCompanyDetails } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: addCompanyDetailsMutation
// ====================================================

export interface addCompanyDetailsMutation {
	addCompanyDetails: boolean;
}

export interface addCompanyDetailsMutationVariables {
	params: addCompanyDetails;
}
