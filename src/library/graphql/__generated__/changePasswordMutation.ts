/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: changePasswordMutation
// ====================================================

export interface changePasswordMutation {
	changePassword: boolean;
}

export interface changePasswordMutationVariables {
	password: string;
}
