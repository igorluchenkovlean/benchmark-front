/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ReqChangeStatusCompany } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: changeStatusCompany
// ====================================================

export interface changeStatusCompany_changeStatusCompany {
	id: number;
	industryName: string;
	companyName: string;
	postalCode: string;
	siteName: string;
	email: string;
	employeesNumber: number;
}

export interface changeStatusCompany {
	changeStatusCompany: changeStatusCompany_changeStatusCompany;
}

export interface changeStatusCompanyVariables {
	params: ReqChangeStatusCompany;
}
