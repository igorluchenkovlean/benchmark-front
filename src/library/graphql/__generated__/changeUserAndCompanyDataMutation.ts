/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ReqUserAndCompanyData } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: changeUserAndCompanyDataMutation
// ====================================================

export interface changeUserAndCompanyDataMutation {
	changeUserAndCompanyData: boolean;
}

export interface changeUserAndCompanyDataMutationVariables {
	params: ReqUserAndCompanyData;
}
