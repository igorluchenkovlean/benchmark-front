/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getCompaniesToApprovalQuery
// ====================================================

export interface getCompaniesToApprovalQuery_getCompaniesToApproval {
	pendingDataRequestId: number;
	companyGroupServiceId: number;
	companyName: string;
	industryName: string;
	groupName: string;
	serviceName: string;
	email: string;
	userId: number;
}

export interface getCompaniesToApprovalQuery {
	getCompaniesToApproval: getCompaniesToApprovalQuery_getCompaniesToApproval[];
}

export interface getCompaniesToApprovalQueryVariables {
	status: string;
}
