/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getCompanyGroupServicesQuery
// ====================================================

export interface getCompanyGroupServicesQuery_getCompanyGroupServices {
	companyGroupServiceId: number;
	serviceName: string;
	isSubmit: boolean;
}

export interface getCompanyGroupServicesQuery {
	getCompanyGroupServices: getCompanyGroupServicesQuery_getCompanyGroupServices[];
}

export interface getCompanyGroupServicesQueryVariables {
	groupId: number;
}
