/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getCompanyGroupsQuery
// ====================================================

export interface getCompanyGroupsQuery_getCompanyGroups {
	companyGroupId: number;
	servicesCount: number;
	serviceGroupName: string;
}

export interface getCompanyGroupsQuery {
	getCompanyGroups: getCompanyGroupsQuery_getCompanyGroups[];
}
