/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getCountries
// ====================================================

export interface getCountries_getCountries {
	name: string;
	code: string;
}

export interface getCountries {
	getCountries: getCountries_getCountries[];
}
