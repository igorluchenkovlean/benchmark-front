/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getCurrentUser
// ====================================================

export interface getCurrentUser_getCurrentUser_user_role {
	id: number;
	label: string;
}

export interface getCurrentUser_getCurrentUser_user_company_industry {
	name: string;
}

export interface getCurrentUser_getCurrentUser_user_company {
	id: number;
	name: string;
	employeesNumber: number | null;
	country: string | null;
	city: string | null;
	postalCode: string | null;
	industry: getCurrentUser_getCurrentUser_user_company_industry | null;
}

export interface getCurrentUser_getCurrentUser_user_language {
	code: number;
	name: string;
}

export interface getCurrentUser_getCurrentUser_user {
	id: number;
	firstName: string;
	lastName: string;
	email: string;
	role: getCurrentUser_getCurrentUser_user_role;
	company: getCurrentUser_getCurrentUser_user_company;
	language: getCurrentUser_getCurrentUser_user_language;
}

export interface getCurrentUser_getCurrentUser_userRegistrationStatus_registrationStatus {
	id: number;
	name: string;
}

export interface getCurrentUser_getCurrentUser_userRegistrationStatus_reservedDomain {
	id: number;
	name: string;
}

export interface getCurrentUser_getCurrentUser_userRegistrationStatus {
	registrationStatus: getCurrentUser_getCurrentUser_userRegistrationStatus_registrationStatus;
	reservedDomain: getCurrentUser_getCurrentUser_userRegistrationStatus_reservedDomain;
	updatedAt: any;
	createdAt: any;
	token: string;
}

export interface getCurrentUser_getCurrentUser {
	user: getCurrentUser_getCurrentUser_user;
	userRegistrationStatus: getCurrentUser_getCurrentUser_userRegistrationStatus;
}

export interface getCurrentUser {
	getCurrentUser: getCurrentUser_getCurrentUser;
}
