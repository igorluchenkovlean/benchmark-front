/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getIndustriesQuery
// ====================================================

export interface getIndustriesQuery_getIndustries {
	id: string;
	name: string;
}

export interface getIndustriesQuery {
	getIndustries: getIndustriesQuery_getIndustries[];
}
