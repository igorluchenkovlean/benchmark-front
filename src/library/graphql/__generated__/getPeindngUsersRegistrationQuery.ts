/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getPeindngUsersRegistrationQuery
// ====================================================

export interface getPeindngUsersRegistrationQuery_getPeindngUsersRegistration {
	id: number;
	industryName: string;
	companyName: string;
	email: string;
	createdAt: any;
}

export interface getPeindngUsersRegistrationQuery {
	getPeindngUsersRegistration: getPeindngUsersRegistrationQuery_getPeindngUsersRegistration[];
}
