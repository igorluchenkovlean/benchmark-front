/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ReqRegisteredUsersCompanies } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getRegisteredCompanies
// ====================================================

export interface getRegisteredCompanies_getRegisteredCompanies {
	id: number;
	industryName: string;
	companyName: string;
	postalCode: string;
	siteName: string;
	email: string;
	employeesNumber: number;
	userId: number;
}

export interface getRegisteredCompanies {
	getRegisteredCompanies: getRegisteredCompanies_getRegisteredCompanies[];
}

export interface getRegisteredCompaniesVariables {
	params: ReqRegisteredUsersCompanies;
}
