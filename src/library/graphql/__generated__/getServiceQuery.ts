/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getServiceQuery
// ====================================================

export interface getServiceQuery_getService {
	id: number;
	companyGroupServiceId: number;
	value: number | null;
	label: string;
}

export interface getServiceQuery {
	getService: getServiceQuery_getService[];
}

export interface getServiceQueryVariables {
	companyGroupServiceId: number;
}
