/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getUserDataQuery
// ====================================================

export interface getUserDataQuery_getUserData_user_role {
	id: number;
	label: string;
}

export interface getUserDataQuery_getUserData_user_company_industry {
	name: string;
}

export interface getUserDataQuery_getUserData_user_company {
	id: number;
	name: string;
	employeesNumber: number | null;
	country: string | null;
	city: string | null;
	postalCode: string | null;
	industry: getUserDataQuery_getUserData_user_company_industry | null;
}

export interface getUserDataQuery_getUserData_user_language {
	code: number;
	name: string;
}

export interface getUserDataQuery_getUserData_user {
	id: number;
	firstName: string;
	lastName: string;
	email: string;
	role: getUserDataQuery_getUserData_user_role;
	company: getUserDataQuery_getUserData_user_company;
	language: getUserDataQuery_getUserData_user_language;
}

export interface getUserDataQuery_getUserData_userRegistrationStatus_registrationStatus {
	id: number;
	name: string;
}

export interface getUserDataQuery_getUserData_userRegistrationStatus_reservedDomain {
	id: number;
	name: string;
}

export interface getUserDataQuery_getUserData_userRegistrationStatus {
	registrationStatus: getUserDataQuery_getUserData_userRegistrationStatus_registrationStatus;
	reservedDomain: getUserDataQuery_getUserData_userRegistrationStatus_reservedDomain;
	updatedAt: any;
	createdAt: any;
}

export interface getUserDataQuery_getUserData {
	user: getUserDataQuery_getUserData_user;
	userRegistrationStatus: getUserDataQuery_getUserData_userRegistrationStatus;
}

export interface getUserDataQuery {
	getUserData: getUserDataQuery_getUserData;
}

export interface getUserDataQueryVariables {
	userId: number;
}
