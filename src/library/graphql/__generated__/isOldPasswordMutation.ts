/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: isOldPasswordMutation
// ====================================================

export interface isOldPasswordMutation {
	isOldPassword: boolean;
}

export interface isOldPasswordMutationVariables {
	password: string;
}
