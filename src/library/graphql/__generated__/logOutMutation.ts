/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: logOutMutation
// ====================================================

export interface logOutMutation {
	logOut: boolean;
}
