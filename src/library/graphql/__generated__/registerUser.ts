/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RegisterUserParams } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: registerUser
// ====================================================

export interface registerUser_registerUser {
	token: string;
	isInitialData: boolean;
}

export interface registerUser {
	registerUser: registerUser_registerUser;
}

export interface registerUserVariables {
	params: RegisterUserParams;
}
