/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RandomUserParams } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: requestToRegister
// ====================================================

export interface requestToRegister {
	requestToRegister: boolean;
}

export interface requestToRegisterVariables {
	params: RandomUserParams;
}
