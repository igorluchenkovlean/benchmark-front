/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdatePassword } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: updatePassword
// ====================================================

export interface updatePassword {
	updatePassword: boolean;
}

export interface updatePasswordVariables {
	params: UpdatePassword;
}
