/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ReqServices } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: updateServiceValueMutation
// ====================================================

export interface updateServiceValueMutation_updateServiceValue {
	companyGroupServiceId: number;
	isSubmit: string;
}

export interface updateServiceValueMutation {
	updateServiceValue: updateServiceValueMutation_updateServiceValue;
}

export interface updateServiceValueMutationVariables {
	params: ReqServices[];
}
