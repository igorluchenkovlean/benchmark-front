/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: uploadFileMutation
// ====================================================

export interface uploadFileMutation_uploadFile {
	companyGroupId: number;
	serviceGroupName: string;
	servicesCount: number;
}

export interface uploadFileMutation {
	uploadFile: uploadFileMutation_uploadFile[];
}

export interface uploadFileMutationVariables {
	file: string;
}
