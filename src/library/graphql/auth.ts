import { gql } from 'apollo-boost';

export const SIGN_MUTATION = gql`
	mutation loginUser($params: LoginUserInput!) {
		loginUser(params: $params) {
			token
			isInitialData
		}
	}
`;

export const LOG_OUT_MUTATION = gql`
	mutation logOutMutation {
		logOut
	}
`;

export const REGISTER_MUTATION = gql`
	mutation registerUser($params: RegisterUserParams!) {
		registerUser(params: $params) {
			token
			isInitialData
		}
	}
`;

export const FORGOT_PASSWORD_MUTATION = gql`
	mutation forgotPassword($email: String!) {
		forgotPassword(email: $email)
	}
`;
export const UPDATE_PASSWORD_MUTATION = gql`
	mutation updatePassword($params: UpdatePassword!) {
		updatePassword(params: $params)
	}
`;

export const REQUEST_TO_REGISTER_MUTATION = gql`
	mutation requestToRegister($params: RandomUserParams!) {
		requestToRegister(params: $params)
	}
`;
