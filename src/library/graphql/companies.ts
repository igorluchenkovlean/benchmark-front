import { gql } from 'apollo-boost';

export const GET_REGISTERED_COMPANIES_QUERY = gql`
	query getRegisteredCompanies($params: ReqRegisteredUsersCompanies!) {
		getRegisteredCompanies(params: $params) {
			id
			industryName
			companyName
			postalCode
			siteName
			email
			employeesNumber
			userId
		}
	}
`;

export const GET_COMPANIES_TO_APPROVAL_QUERY = gql`
	query getCompaniesToApprovalQuery($status: String!) {
		getCompaniesToApproval(status: $status) {
			pendingDataRequestId
			companyGroupServiceId
			companyName
			industryName
			groupName
			serviceName
			email
			userId
		}
	}
`;

export const CHANGE_COMPANIE_STATUS_MUTATION = gql`
	mutation changeStatusCompany($params: ReqChangeStatusCompany!) {
		changeStatusCompany(params: $params) {
			id
			industryName
			companyName
			postalCode
			siteName
			email
			employeesNumber
		}
	}
`;

export const ADD_COMPANY_MUTATION = gql`
	mutation addCompanyMutation($params: addCompany!) {
		addCompany(params: $params)
	}
`;

export const GET_COUNTRIES = gql`
	query getCountries {
		getCountries {
			name
			code
		}
	}
`;

export const GET_PENDING_REGISTRATION = gql`
	query getPeindngUsersRegistrationQuery {
		getPeindngUsersRegistration {
			id
			industryName
			companyName
			email
			createdAt
		}
	}
`;

export const GET_COMPANY_GROUPS_QUERY = gql`
	query getCompanyGroupsQuery {
		getCompanyGroups {
			companyGroupId
			servicesCount
			serviceGroupName
		}
	}
`;

export const GET_COMPANY_GROUPS_SERVICES_QUERY = gql`
	query getCompanyGroupServicesQuery($groupId: Float!) {
		getCompanyGroupServices(groupId: $groupId) {
			companyGroupServiceId
			serviceName
			isSubmit
		}
	}
`;

export const GET_SERVICES_QUERY = gql`
	query getServiceQuery($companyGroupServiceId: Float!) {
		getService(companyGroupServiceId: $companyGroupServiceId) {
			id
			companyGroupServiceId
			value
			label
		}
	}
`;

export const UPDATE_SERVICE_VALUE_MUTATION = gql`
	mutation updateServiceValueMutation($params: [ReqServices!]!) {
		updateServiceValue(params: $params) {
			companyGroupServiceId
			isSubmit
		}
	}
`;

export const UPLOAD_FILE_MUTATION = gql`
	mutation uploadFileMutation($file: String!) {
		uploadFile(file: $file) {
			companyGroupId
			serviceGroupName
			servicesCount
		}
	}
`;

export const GET_INDUSTRIES_QUERY = gql`
	query getIndustriesQuery {
		getIndustries {
			id
			name
		}
	}
`;

export const CHANGE_USER_AND_COMPANY_DATA_MUTATION = gql`
	mutation changeUserAndCompanyDataMutation($params: ReqUserAndCompanyData!) {
		changeUserAndCompanyData(params: $params)
	}
`;
export const CHANGE_PASSWORD_MUTATION = gql`
	mutation changePasswordMutation($password: String!) {
		changePassword(password: $password)
	}
`;
export const IS_OLD_PASSWORD_MUTATION = gql`
	mutation isOldPasswordMutation($password: String!) {
		isOldPassword(password: $password)
	}
`;
