import { gql } from 'apollo-boost';

export const UPDATE_USER_AUTH_DATA = gql`
	mutation UpdateUserAuthData($token: String!, $isInitialData: Boolean!) {
		UpdateUserAuthData(token: $token, isInitialData: $isInitialData) @client
	}
`;

export const LOGOUT_USER = gql`
	mutation LogoutUser {
		logoutUser @client
	}
`;

export const GET_IS_LOGGED_VALUE = gql`
	{
		isLoggedIn @client
	}
`;

export const GET_IS_INITIAL_DATA_VALUE = gql`
	{
		isInitialData @client
	}
`;

export const GET_TOKEN_VALUE = gql`
	{
		token @client
	}
`;

export const UPDATE_USER_DATA = gql`
	mutation UpdateUserData($user: User!) {
		updateUserData(user: $user) @client
	}
`;

export const USER_DATA_QUERY = gql`
	query UserQuery {
		user @client {
			siteName
			email
			firstName
			lastName
			id
			company {
				id
				name
				employeesNumber
				country
				city
				postalCode
				industry {
					name
				}
			}
			language {
				code
				name
			}
			role {
				id
				label
			}
		}
	}
`;
