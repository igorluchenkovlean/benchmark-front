import { IResolvers } from 'graphql-tools';

import { initialState } from 'core/apollo/initialState';

export const resolvers: IResolvers = {
	Mutation: {
		UpdateUserAuthData: (_root, { token, isInitialData }: { token: string; isInitialData: boolean }, { cache }) => {
			const data = {
				token,
				isLoggedIn: true,
				isInitialData,
			};
			cache.writeData({ data });

			return null;
		},

		logoutUser: (_root, _data, { cache }) => {
			const data = {
				initialState,
			};
			cache.writeData({ data });
			window.location.reload();

			return null;
		},

		updateUserData: (_root, { user }, { cache }) => {
			const data = {
				user: {
					...user,
				},
			};
			cache.writeData({ data });

			return null;
		},
	},
};
