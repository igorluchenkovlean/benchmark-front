import { gql } from 'apollo-boost';

export const GET_CURRENT_USER = gql`
	query getCurrentUser {
		getCurrentUser {
			user {
				id
				firstName
				lastName
				email
				role {
					id
					label
				}
				email
				company {
					id
					name
					employeesNumber
					country
					city
					postalCode
					industry {
						name
					}
				}
				language {
					code
					name
				}
			}
			userRegistrationStatus {
				registrationStatus {
					id
					name
				}
				reservedDomain {
					id
					name
				}
				updatedAt
				createdAt
				token
			}
		}
	}
`;

export const GET_USER_DATA_QUERY = gql`
	query getUserDataQuery($userId: Float!) {
		getUserData(userId: $userId) {
			user {
				id
				firstName
				lastName
				email
				role {
					id
					label
				}
				email
				company {
					id
					name
					employeesNumber
					country
					city
					postalCode
					industry {
						name
					}
				}
				language {
					code
					name
				}
			}
			userRegistrationStatus {
				registrationStatus {
					id
					name
				}
				reservedDomain {
					id
					name
				}
				updatedAt
				createdAt
			}
		}
	}
`;

export const ADD_COMNPANY_DETAILS_MUTATION = gql`
	mutation addCompanyDetailsMutation($params: addCompanyDetails!) {
		addCompanyDetails(params: $params)
	}
`;
