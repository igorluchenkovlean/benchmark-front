import Storage from './storage';
import { GraphqlMutationLocal } from 'library/common/types/graphql';

interface IStorage {
	token: string;
	isInitialData: boolean;
}

export const getUser: () => IStorage | null = () => Storage.getItem('user');

export const getToken: () => string | null = () => {
	const user = getUser();
	if (user) return user.token;

	return null;
};

export const checkToken = (userMutation: GraphqlMutationLocal) => {
	const user = getUser();

	if (user) {
		userMutation({
			variables: {
				...user,
			},
		});
	}
};

export const loginUserLocal = async ({
	updateUserMutation,
	...rest
}: {
	updateUserMutation: GraphqlMutationLocal;
	token: string;
	isInitialData: boolean;
}) => {
	updateUserMutation({
		variables: {
			...rest,
		},
	});
	Storage.setItem('user', rest as any);
};

export const logout = ({ logoutUser }: { logoutUser: GraphqlMutationLocal }) => {
	Storage.removeItem('user');
	logoutUser();
};
