import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';
import Modal from 'react-modal';

import { forgotPassword, forgotPasswordVariables } from 'library/graphql/__generated__/forgotPassword';
import logoBig from 'resources/icons/logo-big.svg';
import businessHands from 'resources/images/business-hands.png';
import { FORGOT_PASSWORD_MUTATION } from 'library/graphql/auth';
import ErrorMessage from 'library/common/components/ErrorMessage';
import Input from 'library/common/components/Inputs/Input';
import Button from 'library/common/components/Button/Button';
import WithLoader from 'library/common/components/WithLoader';
import { submitForgotPasswordHandler } from './utils/forgotUtils';

import styles from './forgotPassword.module.scss';

const ForgotPassword: React.FC = () => {
	const { t } = useTranslation();

	const [modalIsOpen, setModalIsOpen] = useState(false);
	const [email, setEmail] = useState('');
	const [emailError, setEmailError] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [forgotPassworMutation] = useMutation<forgotPassword, forgotPasswordVariables>(FORGOT_PASSWORD_MUTATION);

	const submitForgotPassword = () =>
		submitForgotPasswordHandler({
			email,
			setEmailError,
			setIsLoading,
			forgotPassworMutation,
			setModalIsOpen,
			t,
		});

	const errors = [emailError];

	return (
		<>
			<Modal
				isOpen={modalIsOpen}
				contentLabel='Example Modal'
				className={styles.modal}
				overlayClassName={styles.modalOverlay}
			>
				<div className={styles.popupWrapper}>
					<div className={styles.popupText}>{t('Forgot.Password reset successfully, please check your email')}</div>
					<Button onClick={() => setModalIsOpen(false)}> Ok</Button>
				</div>
			</Modal>
			<div className={styles.forgotContainer}>
				<div className={styles.imageContainer}>
					<img className={styles.image} src={businessHands} alt='' />
				</div>
				<WithLoader isLoading={isLoading}>
					<div className={styles.contentContainer}>
						<div>
							<div className={styles.title}>{t('Login.Welcome to')}</div>
							<div className={styles.logoContainer}>
								<img className={styles.logo} src={logoBig} alt='' />
							</div>
							<div className={styles.subTitle}>
								{t('Forgot.Enter your registered email to receive link to reset password')}
							</div>
							<Input
								label={t('Login.Email')}
								placeholder={t('Login.Enter your email here')}
								value={email}
								onChange={setEmail}
								error={emailError}
							/>
						</div>
						<div className={styles.button}>
							<ErrorMessage errors={errors} />

							<Button onClick={submitForgotPassword} isFullwidth>
								{t('Forgot.Send')}
							</Button>
						</div>
					</div>
				</WithLoader>
			</div>
		</>
	);
};

export default ForgotPassword;
