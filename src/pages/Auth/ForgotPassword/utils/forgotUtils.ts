import { TFunction } from 'i18next';

import { validateEmail } from 'library/utilities/validation';
import { GraphqlMutation } from 'library/common/types/graphql';
import { forgotPassword, forgotPasswordVariables } from 'library/graphql/__generated__/forgotPassword';

export const submitForgotPasswordHandler = async ({
	email,
	setEmailError,
	setIsLoading,
	forgotPassworMutation,
	setModalIsOpen,
	t,
}: {
	email: string;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	forgotPassworMutation: GraphqlMutation<forgotPassword, forgotPasswordVariables>;
	setModalIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
	t: TFunction;
}) => {
	const validationResult = validateForgotPasswordFields({
		email,
		setEmailError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await forgotPassworMutation({
			variables: { email: email.trim() },
		});
		if (data && data.forgotPassword) {
			setModalIsOpen(true);
		} else {
			setEmailError(t('Errors.User does not exist'));
		}
	} catch ({ graphQLErrors }) {
		if (graphQLErrors && graphQLErrors.length && graphQLErrors[0].message === "User doesn't exists") {
			setEmailError(t('Errors.User does not exist'));
		}
	} finally {
		setIsLoading(false);
	}
};

export const validateForgotPasswordFields = ({
	email,
	setEmailError,
	t,
}: {
	email: string;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;
	if (email.trim().length === 0) {
		setEmailError(t('Errors.Email cannot be blank'));
		isSuccess = false;
	} else if (!validateEmail(email)) {
		isSuccess = false;
		setEmailError(t('Errors.Email is incorrect'));
	} else {
		setEmailError('');
	}

	return isSuccess;
};
