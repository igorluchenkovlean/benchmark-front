import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { loginUser, loginUserVariables } from 'library/graphql/__generated__/loginUser';
import logoBig from 'resources/icons/logo-big.svg';
import businessHands from 'resources/images/business-hands.png';
import { SIGN_MUTATION } from 'library/graphql/auth';
import { UPDATE_USER_AUTH_DATA } from 'library/graphql/localData/localData';
import ErrorMessage from 'library/common/components/ErrorMessage';
import Input from 'library/common/components/Inputs/Input';
import Button from 'library/common/components/Button/Button';
import WithLoader from 'library/common/components/WithLoader';
import { submitLoginHandler } from './utils/loginUtils';

import styles from './login.module.scss';

const Login: React.FC = () => {
	const { t } = useTranslation();
	const history = useHistory();

	const [email, setEmail] = useState('');
	const [emailError, setEmailError] = useState('');
	const [password, setPassword] = useState('');
	const [passwordError, setPasswordError] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [loginMutation] = useMutation<loginUser, loginUserVariables>(SIGN_MUTATION);

	const [updateUserMutation] = useMutation(UPDATE_USER_AUTH_DATA);
	const submitLogin = () =>
		submitLoginHandler({
			email,
			setEmailError,
			password,
			setPasswordError,
			setIsLoading,
			loginMutation,
			updateUserMutation,
			t,
		});

	const errors = [emailError, passwordError];
	const forgotPasswordHandler = () => history.push('forgot-password');
	const registerHandler = () => history.push('register-random');

	return (
		<>
			<div className={styles.loginContainer}>
				<div className={styles.imageContainer}>
					<img className={styles.image} src={businessHands} alt='' />
				</div>
				<WithLoader isLoading={isLoading}>
					<div className={styles.contentContainer}>
						<div>
							<div className={styles.title}>{t('Login.Welcome to')}</div>
							<div className={styles.logoContainer}>
								<img className={styles.logo} src={logoBig} alt='' />
							</div>
							<Input
								label={t('Login.Email')}
								placeholder={t('Login.Enter your email here')}
								value={email}
								onChange={setEmail}
								error={emailError}
							/>

							<Input
								label={t('Login.Password')}
								placeholder={t('Login.Enter your password here')}
								type='password'
								rightLabel={t('Login.Forgot password?')}
								value={password}
								onChange={setPassword}
								error={passwordError}
								onClick={forgotPasswordHandler}
							/>
						</div>
						<div>
							<ErrorMessage errors={errors} />
							<Button onClick={submitLogin} isFullwidth>
								{t('Login.Login')}
							</Button>

							<div className={styles.registerContainer}>
								<span className={styles.registerCommon}>{t('Login.Dont have an account?')}</span>
								<span className={styles.registerCommon}> {t('Login.Click here to')}</span>
								<span className={styles.register} onClick={registerHandler}>
									{t('Login.Register')}
								</span>
							</div>
						</div>
					</div>
				</WithLoader>
			</div>
		</>
	);
};

export default Login;
