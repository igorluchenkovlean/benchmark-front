import { TFunction } from 'i18next';

import { loginUserLocal } from 'library/utilities/token';
import { validateEmail } from 'library/utilities/validation';
import { GraphqlMutation, GraphqlMutationLocal } from 'library/common/types/graphql';
import { loginUser, loginUserVariables } from 'library/graphql/__generated__/loginUser';

export const submitLoginHandler = async ({
	email,
	setEmailError,
	password,
	setPasswordError,
	setIsLoading,
	loginMutation,
	updateUserMutation,
	t,
}: {
	email: string;
	password: string;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordError: React.Dispatch<React.SetStateAction<string>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	loginMutation: GraphqlMutation<loginUser, loginUserVariables>;
	updateUserMutation: GraphqlMutationLocal;
	t: TFunction;
}) => {
	const validationResult = validateLoginFields({
		email,
		setEmailError,
		password,
		setPasswordError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await loginMutation({
			variables: { params: { email: email.trim(), password: password.trim() } },
		});

		if (data && data.loginUser.token) {
			loginUserLocal({ updateUserMutation, ...data.loginUser });
		} else {
			setEmailError(t('Errors.User does not exist'));
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
		if (graphQLErrors.length && graphQLErrors[0].message === 'User not found') {
			setEmailError(t('Errors.User not found'));
		} else if (graphQLErrors.length && graphQLErrors[0].message === 'Invalid credentials') {
			setPasswordError(t('Errors.Password or email wrong'));
		}
	} finally {
		setIsLoading(false);
	}
};

export const validateLoginFields = ({
	email,
	setEmailError,
	password,
	setPasswordError,
	t,
}: {
	email: string;
	password: string;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;
	if (email.trim().length === 0) {
		setEmailError(t('Errors.Email cannot be blank'));
		isSuccess = false;
	} else if (!validateEmail(email)) {
		isSuccess = false;
		setEmailError(t('Errors.Email is incorrect'));
	} else {
		setEmailError('');
	}

	if (password.trim().length === 0) {
		setPasswordError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else {
		setPasswordError('');
	}

	return isSuccess;
};
