import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router';
import Modal from 'react-modal';

import { registerUser, registerUserVariables } from 'library/graphql/__generated__/registerUser';
import logoBig from 'resources/icons/logo-big.svg';
import businessHands from 'resources/images/business-hands.png';
import { REGISTER_MUTATION } from 'library/graphql/auth';
import { UPDATE_USER_AUTH_DATA } from 'library/graphql/localData/localData';
import ErrorMessage from 'library/common/components/ErrorMessage';
import Input from 'library/common/components/Inputs/Input';
import Button from 'library/common/components/Button/Button';
import Checkbox from 'library/common/components/Checkbox';
import WithLoader from 'library/common/components/WithLoader';
import Terms from 'pages/TermsAndConditions';
import { submitRegisterHandler } from './utils/registerUtils';

import styles from './register.module.scss';

const Register: React.FC = () => {
	const { t } = useTranslation();
	const location = useLocation();

	const [termsChecked, setTermsChecked] = useState(false);
	const [termsCheckedError, setTermsCheckedError] = useState('');
	const [modalIsOpen, setModalIsOpen] = useState(false);
	const [firstName, setFirstName] = useState('');
	const [firstNameError, setFirstNameError] = useState('');
	const [lastName, setLastName] = useState('');
	const [lastNameError, setLastNameError] = useState('');
	const [password, setPassword] = useState('');
	const [passwordError, setPasswordError] = useState('');
	const [passwordSecond, setPasswordSecond] = useState('');
	const [passwordSecondError, setPasswordSecondError] = useState('');
	const [token, setToken] = useState<string>(location.pathname.substr(10));
	const [isLoading, setIsLoading] = useState(false);
	const [registerMutation] = useMutation<registerUser, registerUserVariables>(REGISTER_MUTATION);

	const [updateUserMutation] = useMutation(UPDATE_USER_AUTH_DATA);
	const submitRegister = () =>
		submitRegisterHandler({
			firstName,
			setFirstNameError,
			lastName,
			setLastNameError,
			password,
			setPasswordError,
			passwordSecond,
			setPasswordSecondError,
			setIsLoading,
			registerMutation,
			token,
			termsChecked,
			setTermsCheckedError,
			updateUserMutation,
			t,
		});

	const errors = [firstNameError, lastNameError, passwordError, passwordSecondError, termsCheckedError];

	const termsHandler = () => setModalIsOpen(true);

	return (
		<>
			<Modal
				isOpen={modalIsOpen}
				contentLabel='Example Modal'
				className={styles.modal}
				overlayClassName={styles.modalOverlay}
			>
				<Terms setModalIsOpen={setModalIsOpen} setTermsChecked={setTermsChecked} />
			</Modal>
			<div className={styles.registerContainer}>
				<div className={styles.imageContainer}>
					<img className={styles.image} src={businessHands} alt='' />
				</div>
				<WithLoader isLoading={isLoading}>
					<div className={styles.contentContainer}>
						<div className={styles.title}>{t('Register.Hello, Welcome to')}</div>
						<div className={styles.logoContainer}>
							<img className={styles.logo} src={logoBig} alt='' />
						</div>

						<Input
							label={t('Register.First Name')}
							placeholder={t('Register.Enter your first name here')}
							value={firstName}
							onChange={setFirstName}
							error={firstNameError}
						/>
						<Input
							label={t('Register.Last Name')}
							placeholder={t('Register.Enter your last name here')}
							value={lastName}
							onChange={setLastName}
							error={lastNameError}
						/>

						<Input
							label={t('Register.Password')}
							placeholder={t('Register.Enter your password')}
							type='password'
							value={password}
							onChange={setPassword}
							error={passwordError}
						/>

						<Input
							label={t('Register.Re-enter your password')}
							placeholder={t('Register.Re-enter your password')}
							type='password'
							value={passwordSecond}
							onChange={setPasswordSecond}
							error={passwordSecondError}
						/>
						<div className={styles.termsContainer}>
							<div className={styles.checkbox} onClick={() => setTermsChecked(!termsChecked)}>
								<Checkbox checked={termsChecked} />
							</div>

							<div className={styles.termsCommon}> {t('Register.I agree to your')}</div>

							<div className={styles.terms} onClick={termsHandler}>
								{t('Register.Terms & Condition')}
							</div>
						</div>
						<div className={styles.button}>
							<ErrorMessage errors={errors} />
							<Button onClick={submitRegister} isFullwidth>
								{t('Register.Register')}
							</Button>
						</div>
					</div>
				</WithLoader>
			</div>
		</>
	);
};

export default Register;
