import { TFunction } from 'i18next';

import { loginUserLocal } from 'library/utilities/token';
import { GraphqlMutation, GraphqlMutationLocal, UserLanguage } from 'library/common/types/graphql';
import { registerUser, registerUserVariables } from 'library/graphql/__generated__/registerUser';

export const submitRegisterHandler = async ({
	firstName,
	setFirstNameError,
	lastName,
	setLastNameError,
	password,
	setPasswordError,
	passwordSecond,
	setPasswordSecondError,
	setIsLoading,
	registerMutation,
	token,
	termsChecked,
	setTermsCheckedError,
	updateUserMutation,
	t,
}: {
	firstName: string;
	lastName: string;
	password: string;
	passwordSecond: string;
	setFirstNameError: React.Dispatch<React.SetStateAction<string>>;
	setLastNameError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordSecondError: React.Dispatch<React.SetStateAction<string>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	registerMutation: GraphqlMutation<registerUser, registerUserVariables>;
	token: string;
	termsChecked: boolean;
	setTermsCheckedError: React.Dispatch<React.SetStateAction<string>>;
	updateUserMutation: GraphqlMutationLocal;
	t: TFunction;
}) => {
	const validationResult = validateRegisterFields({
		firstName,
		setFirstNameError,
		lastName,
		setLastNameError,
		password,
		setPasswordError,
		passwordSecond,
		setPasswordSecondError,
		termsChecked,
		setTermsCheckedError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await registerMutation({
			variables: {
				params: {
					firstName: firstName.trim(),
					lastName: lastName.trim(),
					password: password.trim(),
					token,
					language: UserLanguage.en,
				},
			},
		});

		if (data && data.registerUser.token) {
			loginUserLocal({ updateUserMutation, ...data.registerUser });
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateRegisterFields = ({
	firstName,
	setFirstNameError,
	lastName,
	setLastNameError,
	password,
	setPasswordError,
	passwordSecond,
	setPasswordSecondError,
	termsChecked,
	setTermsCheckedError,
	t,
}: {
	firstName: string;
	lastName: string;
	password: string;
	passwordSecond: string;
	setFirstNameError: React.Dispatch<React.SetStateAction<string>>;
	setLastNameError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordSecondError: React.Dispatch<React.SetStateAction<string>>;
	termsChecked: boolean;
	setTermsCheckedError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;
	if (!termsChecked) {
		setTermsCheckedError(t('Errors.You need read and agree with Terms&Conditions'));
		isSuccess = false;
	} else {
		setTermsCheckedError('');
	}

	if (firstName.trim().length === 0) {
		setFirstNameError(t('Errors.First name cannot be blank'));
		isSuccess = false;
	} else {
		setFirstNameError('');
	}

	if (lastName.trim().length === 0) {
		setLastNameError(t('Errors.Last name cannot be blank'));
		isSuccess = false;
	} else {
		setLastNameError('');
	}

	if (password.trim().length === 0) {
		setPasswordError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else {
		setPasswordError('');
	}

	if (passwordSecond.trim().length === 0) {
		setPasswordSecondError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else if (passwordSecond.trim() !== password.trim()) {
		setPasswordSecondError(t('Errors.Passwords must match'));
		isSuccess = false;
	} else {
		setPasswordSecondError('');
	}

	return isSuccess;
};
