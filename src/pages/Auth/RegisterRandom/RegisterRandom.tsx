import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';
import Modal from 'react-modal';
import { useHistory } from 'react-router-dom';

import { requestToRegister, requestToRegisterVariables } from 'library/graphql/__generated__/requestToRegister';
import logoBig from 'resources/icons/logo-big.svg';
import warningBlue from 'resources/icons/warningBlue.svg';
import businessHands from 'resources/images/business-hands.png';
import { REQUEST_TO_REGISTER_MUTATION } from 'library/graphql/auth';
import ErrorMessage from 'library/common/components/ErrorMessage';
import Input from 'library/common/components/Inputs/Input';
import Button from 'library/common/components/Button/Button';
import Checkbox from 'library/common/components/Checkbox';
import WithLoader from 'library/common/components/WithLoader';
import Terms from 'pages/TermsAndConditions';
import { submitRegisterRandomHandler, validateRegisterRandomFields } from './utils/registerRandomUtils';

import styles from './registerRandom.module.scss';

const RegisterRandom: React.FC = () => {
	const { t } = useTranslation();
	const history = useHistory();

	const [termsChecked, setTermsChecked] = useState(false);
	const [termsCheckedError, setTermsCheckedError] = useState('');
	const [termsModalIsOpen, setTermsModalIsOpen] = useState(false);
	const [submitModalIsOpen, setSubmitModalIsOpen] = useState(false);
	const [firstName, setFirstName] = useState('');
	const [firstNameError, setFirstNameError] = useState('');
	const [lastName, setLastName] = useState('');
	const [lastNameError, setLastNameError] = useState('');

	const [email, setEmail] = useState('');
	const [emailError, setEmailError] = useState('');

	const [companyName, setCompanyName] = useState('');
	const [companyNameError, setCompanyNameError] = useState('');

	const [isLoading, setIsLoading] = useState(false);
	const [registerRandomMutation] = useMutation<requestToRegister, requestToRegisterVariables>(
		REQUEST_TO_REGISTER_MUTATION,
	);

	const checkRegisterValidation = () => {
		const validationResult = validateRegisterRandomFields({
			firstName,
			setFirstNameError,
			lastName,
			setLastNameError,
			email,
			setEmailError,
			companyName,
			setCompanyNameError,
			termsChecked,
			setTermsCheckedError,
			t,
		});
		if (!validationResult) return;
		setSubmitModalIsOpen(true);
	};

	const submitRegister = () => {
		submitRegisterRandomHandler({
			firstName,
			lastName,
			email,
			companyName,
			setIsLoading,
			registerRandomMutation,
			history,
		});
		setSubmitModalIsOpen(false);
	};

	const errors = [firstNameError, lastNameError, emailError, companyNameError, termsCheckedError];

	const termsHandler = () => setTermsModalIsOpen(true);

	return (
		<>
			<Modal isOpen={termsModalIsOpen} className={styles.modal} overlayClassName={styles.modalOverlay}>
				<Terms setModalIsOpen={setTermsModalIsOpen} setTermsChecked={setTermsChecked} />
			</Modal>
			<Modal isOpen={submitModalIsOpen} className={styles.modalPopup} overlayClassName={styles.modalOverlayPopup}>
				<div className={styles.popupWrapper}>
					<div className={styles.popupImageWrapper}>
						<img className={styles.popupImage} src={warningBlue} alt='' />
					</div>
					<div className={styles.popupText}>
						<div>{t('Register.Right now your company is not registered with us')}</div>
						<div>{t('Register.If you agree we can use your provided information to contact you later')}</div>
					</div>
					<div className={styles.buttonContainer}>
						<Button type={'red'} onClick={() => setSubmitModalIsOpen(false)}>
							{t('Terms.Cancel')}
						</Button>
						<Button onClick={submitRegister}> {t('Terms.I Agree')}</Button>
					</div>
				</div>
			</Modal>
			<div className={styles.registerContainer}>
				<div className={styles.imageContainer}>
					<img className={styles.image} src={businessHands} alt='' />
				</div>
				<WithLoader isLoading={isLoading}>
					<div className={styles.contentContainer}>
						<div className={styles.title}>{t('Register.Hello, Welcome to')}</div>
						<div className={styles.logoContainer}>
							<img className={styles.logo} src={logoBig} alt='' />
						</div>

						<Input
							label={t('Register.First Name')}
							placeholder={t('Register.Enter your first name here')}
							value={firstName}
							onChange={setFirstName}
							error={firstNameError}
						/>
						<Input
							label={t('Register.Last Name')}
							placeholder={t('Register.Enter your last name here')}
							value={lastName}
							onChange={setLastName}
							error={lastNameError}
						/>

						<Input
							label={t('Login.Email')}
							placeholder={t('Login.Enter your email here')}
							value={email}
							onChange={setEmail}
							error={emailError}
						/>

						<Input
							label={t('Register.Company Name')}
							placeholder={t('Register.Enter your company name here')}
							value={companyName}
							onChange={setCompanyName}
							error={companyNameError}
						/>
						<div className={styles.termsContainer}>
							<div className={styles.checkbox} onClick={() => setTermsChecked(!termsChecked)}>
								<Checkbox checked={termsChecked} />
							</div>

							<div className={styles.termsCommon}> {t('Register.I agree to your')}</div>

							<div className={styles.terms} onClick={termsHandler}>
								{t('Register.Terms & Condition')}
							</div>
						</div>
						<div className={styles.button}>
							<ErrorMessage errors={errors} />
							<Button onClick={checkRegisterValidation} isFullwidth>
								{t('Register.Register')}
							</Button>
						</div>
					</div>
				</WithLoader>
			</div>
		</>
	);
};

export default RegisterRandom;
