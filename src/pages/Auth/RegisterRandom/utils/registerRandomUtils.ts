import { RouteComponentProps } from 'react-router-dom';
import { TFunction } from 'i18next';

import { GraphqlMutation } from 'library/common/types/graphql';
import { validateEmail } from 'library/utilities/validation';
import { requestToRegister, requestToRegisterVariables } from 'library/graphql/__generated__/requestToRegister';

export const submitRegisterRandomHandler = async ({
	firstName,
	lastName,
	email,
	companyName,
	setIsLoading,
	registerRandomMutation,
	history,
}: {
	history: RouteComponentProps['history'];
	firstName: string;
	lastName: string;
	email: string;
	companyName: string;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	registerRandomMutation: GraphqlMutation<requestToRegister, requestToRegisterVariables>;
}) => {
	setIsLoading(true);
	try {
		const { data } = await registerRandomMutation({
			variables: {
				params: {
					firstName: firstName.trim(),
					lastName: lastName.trim(),
					userEmail: email.trim(),
					companyName,
				},
			},
		});

		if (data && data.requestToRegister) {
			history.push('/login');
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateRegisterRandomFields = ({
	firstName,
	setFirstNameError,
	lastName,
	setLastNameError,
	email,
	setEmailError,
	companyName,
	setCompanyNameError,
	termsChecked,
	setTermsCheckedError,
	t,
}: {
	firstName: string;
	lastName: string;
	email: string;
	companyName: string;
	setFirstNameError: React.Dispatch<React.SetStateAction<string>>;
	setLastNameError: React.Dispatch<React.SetStateAction<string>>;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	setCompanyNameError: React.Dispatch<React.SetStateAction<string>>;
	termsChecked: boolean;
	setTermsCheckedError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;
	if (!termsChecked) {
		setTermsCheckedError(t('Errors.You need read and agree with Terms&Conditions'));
		isSuccess = false;
	} else {
		setTermsCheckedError('');
	}

	if (firstName.trim().length === 0) {
		setFirstNameError(t('Errors.First name cannot be blank'));
		isSuccess = false;
	} else {
		setFirstNameError('');
	}

	if (lastName.trim().length === 0) {
		setLastNameError(t('Errors.Last name cannot be blank'));
		isSuccess = false;
	} else {
		setLastNameError('');
	}

	if (email.trim().length === 0) {
		setEmailError(t('Errors.Email cannot be blank'));
		isSuccess = false;
	} else if (!validateEmail(email)) {
		isSuccess = false;
		setEmailError(t('Errors.Email is incorrect'));
	} else {
		setEmailError('');
	}

	if (companyName.trim().length === 0) {
		setCompanyNameError(t('Errors.Company name cannot be blank'));
		isSuccess = false;
	} else {
		setCompanyNameError('');
	}

	return isSuccess;
};
