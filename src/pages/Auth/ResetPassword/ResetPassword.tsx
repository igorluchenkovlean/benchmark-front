import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router';
import { useHistory } from 'react-router-dom';

import { updatePassword, updatePasswordVariables } from 'library/graphql/__generated__/updatePassword';
import logoBig from 'resources/icons/logo-big.svg';
import businessHands from 'resources/images/business-hands.png';
import { UPDATE_PASSWORD_MUTATION } from 'library/graphql/auth';
import ErrorMessage from 'library/common/components/ErrorMessage';
import Input from 'library/common/components/Inputs/Input';
import Button from 'library/common/components/Button/Button';
import WithLoader from 'library/common/components/WithLoader';
import { submitUpdatePasswordHandler } from './utils/resetUtils';

import styles from './resetPassword.module.scss';

const ResetPassword: React.FC = () => {
	const { t } = useTranslation();
	const location = useLocation();
	const history = useHistory();
	const [password, setPassword] = useState('');
	const [passwordError, setPasswordError] = useState('');
	const [passwordSecond, setPasswordSecond] = useState('');
	const [passwordSecondError, setPasswordSecondError] = useState('');
	const [token, setToken] = useState<string>(location.pathname.substr(16));
	const [isLoading, setIsLoading] = useState(false);
	const [updatePasswordMutation] = useMutation<updatePassword, updatePasswordVariables>(UPDATE_PASSWORD_MUTATION);

	const submitUpdatePassword = () =>
		submitUpdatePasswordHandler({
			password,
			setPasswordError,
			passwordSecond,
			setPasswordSecondError,
			setIsLoading,
			updatePasswordMutation,
			token,
			history,
			t,
		});

	const errors = [passwordError, passwordSecondError];

	return (
		<div className={styles.resetContainer}>
			<div className={styles.imageContainer}>
				<img className={styles.image} src={businessHands} alt='' />
			</div>
			<WithLoader isLoading={isLoading}>
				<div className={styles.contentContainer}>
					<div>
						<div className={styles.title}>{t('Login.Welcome to')}</div>
						<div className={styles.logoContainer}>
							<img className={styles.logo} src={logoBig} alt='' />
						</div>
						<div className={styles.subTitle}>{t('Reset.Reset password')}</div>
						<Input
							label={t('Login.Password')}
							placeholder={t('Login.Enter your password')}
							value={password}
							type='password'
							onChange={setPassword}
							error={passwordError}
						/>
						<Input
							label={t('Register.Re-enter your password')}
							placeholder={t('Register.Re-enter your password')}
							value={passwordSecond}
							type='password'
							onChange={setPasswordSecond}
							error={passwordSecondError}
						/>
					</div>
					<div className={styles.button}>
						<ErrorMessage errors={errors} />
						<Button onClick={submitUpdatePassword} isFullwidth>
							{t('Reset.Update Password')}
						</Button>
					</div>
				</div>
			</WithLoader>
		</div>
	);
};

export default ResetPassword;
