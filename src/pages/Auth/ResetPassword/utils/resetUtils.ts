import { RouteComponentProps } from 'react-router-dom';
import { TFunction } from 'i18next';

import { GraphqlMutation } from 'library/common/types/graphql';
import { updatePassword, updatePasswordVariables } from 'library/graphql/__generated__/updatePassword';

export const submitUpdatePasswordHandler = async ({
	password,
	setPasswordError,
	passwordSecond,
	setPasswordSecondError,
	setIsLoading,
	updatePasswordMutation,
	token,
	history,
	t,
}: {
	password: string;
	setPasswordError: React.Dispatch<React.SetStateAction<string>>;
	passwordSecond: string;
	setPasswordSecondError: React.Dispatch<React.SetStateAction<string>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	updatePasswordMutation: GraphqlMutation<updatePassword, updatePasswordVariables>;
	token: string;
	history: RouteComponentProps['history'];
	t: TFunction;
}) => {
	const validationResult = validateUpdatePasswordFields({
		password,
		setPasswordError,
		passwordSecond,
		setPasswordSecondError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await updatePasswordMutation({
			variables: {
				params: {
					password: password.trim(),
					token,
				},
			},
		});
		if (data && data.updatePassword) {
			history.push('/login');
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateUpdatePasswordFields = ({
	password,
	setPasswordError,
	passwordSecond,
	setPasswordSecondError,
	t,
}: {
	password: string;
	passwordSecond: string;
	setPasswordError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordSecondError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;
	if (password.trim().length === 0) {
		setPasswordError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else {
		setPasswordError('');
	}

	if (passwordSecond.trim().length === 0) {
		setPasswordSecondError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else if (passwordSecond.trim() !== password.trim()) {
		setPasswordSecondError(t('Errors.Passwords must match'));
		isSuccess = false;
	} else {
		setPasswordSecondError('');
	}

	return isSuccess;
};
