import React, { useState, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';
import Select from 'react-select';

import { getCountries } from 'library/graphql/__generated__/getCountries';
import {
	addCompanyDetailsMutation,
	addCompanyDetailsMutationVariables,
} from 'library/graphql/__generated__/addCompanyDetailsMutation';
import businessHands from 'resources/images/business-hands.png';
import warning from 'resources/icons/warning.svg';
import { GET_COUNTRIES } from 'library/graphql/companies';
import { UPDATE_USER_AUTH_DATA } from 'library/graphql/localData/localData';
import { ADD_COMNPANY_DETAILS_MUTATION } from 'library/graphql/user';
import ErrorMessage from 'library/common/components/ErrorMessage';
import Input from 'library/common/components/Inputs/Input';
import Button from 'library/common/components/Button/Button';
import WithLoader from 'library/common/components/WithLoader';
import { submitNextHandler } from './utils/companyDetailsUtils';
import { reactSelectStyles } from './utils/reactSelectStyles';

import styles from './companyDetails.module.scss';

export interface IOptionsCoutries {
	value: string;
	label: string;
}

const CompanyDetails: React.FC = () => {
	const { t } = useTranslation();

	const [optionsCoutries, setOptionsCoutries] = useState<IOptionsCoutries[]>([]);
	const [siteName, setSiteName] = useState('');
	const [siteNameError, setSiteNameError] = useState('');
	const [numbersOfEmployees, setNumbersOfEmployees] = useState('');
	const [numbersOfEmployeesError, setNumbersOfEmployeesError] = useState('');
	const [country, setCountry] = useState('');
	const [countryError, setCountryError] = useState('');
	const [city, setCity] = useState('');
	const [cityError, setCityError] = useState('');
	const [postalCode, setPostalCode] = useState('');
	const [postalCodeError, setPostalCodeError] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [nextMutation] = useMutation<addCompanyDetailsMutation, addCompanyDetailsMutationVariables>(
		ADD_COMNPANY_DETAILS_MUTATION,
	);

	const { loading: industriesLoading, data: countriesData } = useQuery<getCountries>(GET_COUNTRIES);

	useEffect(() => {
		if (countriesData) {
			const arrayOfIndustries = countriesData.getCountries.map(val => ({
				value: val.name,
				label: val.name,
			}));
			setOptionsCoutries(arrayOfIndustries);
		}
	}, [countriesData]);

	const [updateUserMutation] = useMutation(UPDATE_USER_AUTH_DATA);

	const submitNext = () =>
		submitNextHandler({
			siteName,
			setSiteNameError,
			numbersOfEmployees,
			setNumbersOfEmployeesError,
			country,
			setCountryError,
			city,
			setCityError,
			postalCode,
			setPostalCodeError,
			setIsLoading,
			nextMutation,
			updateUserMutation,
			t,
		});

	const handleChangeCoutrySelect = (e: any) => {
		if (countryError) {
			setCountryError('');
		}
		setCountry(e.value);
	};

	const setPostalCodeHandler = (value: string) => {
		if (value.length > 6) return;
		setPostalCode(value);
	};

	const setNumbersOfEmployeesHandler = (value: string) => {
		if (value.length > 10) return;
		setNumbersOfEmployees(value);
	};

	const errors = [siteNameError, numbersOfEmployeesError, countryError, cityError, postalCodeError];

	return (
		<div className={styles.companyDetailsContainer}>
			<div className={styles.imageContainer}>
				<img className={styles.image} src={businessHands} alt='' />
			</div>
			<WithLoader isLoading={isLoading}>
				<div className={styles.contentContainer}>
					<div className={styles.title}>{t('Companie Details.We just need few details regarding your site')}</div>

					<Input
						label={t('Companie Details.Site Name')}
						placeholder={t('Companie Details.Enter your site name')}
						value={siteName}
						onChange={setSiteName}
						error={siteNameError}
					/>
					<Input
						label={t('Companie Details.Number of employees')}
						placeholder={t('Companie Details.Enter number of employees in your site')}
						isNumber
						value={numbersOfEmployees}
						onChange={setNumbersOfEmployeesHandler}
						error={numbersOfEmployeesError}
					/>

					<div className={styles.selectCountry}>
						<div className={styles.labelCountry}>{t('Companie Details.Country')}</div>
						<div className={styles.inputContainer}>
							<Select
								value={optionsCoutries && optionsCoutries.filter((obj: IOptionsCoutries) => obj.value === country)}
								options={optionsCoutries}
								styles={reactSelectStyles}
								placeholder={t('Companie Details.Select country')}
								noOptionsMessage={() => t('Companie Details.No Such Country')}
								onChange={handleChangeCoutrySelect}
							/>
							{countryError && <img className={styles.warning} src={warning} alt='' />}
						</div>
					</div>

					<Input
						label={t('Companie Details.City')}
						placeholder={t('Companie Details.Enter your city')}
						value={city}
						onChange={setCity}
						error={cityError}
					/>

					<Input
						label={t('Companie Details.Postal code')}
						placeholder={t('Companie Details.Enter your postal code')}
						isNumber
						value={postalCode}
						onChange={setPostalCodeHandler}
						error={postalCodeError}
					/>

					<div className={styles.button}>
						<ErrorMessage errors={errors} />
						<Button onClick={submitNext} isFullwidth>
							{t('Companie Details.Next')}
						</Button>
					</div>
				</div>
			</WithLoader>
		</div>
	);
};

export default CompanyDetails;
