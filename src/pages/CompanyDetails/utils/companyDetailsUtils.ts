import { TFunction } from 'i18next';

import { getToken } from 'library/utilities/token';
import { GraphqlMutation, GraphqlMutationLocal } from 'library/common/types/graphql';
import {
	addCompanyDetailsMutation,
	addCompanyDetailsMutationVariables,
} from 'library/graphql/__generated__/addCompanyDetailsMutation';
import Storage from 'library/utilities/storage';

export const submitNextHandler = async ({
	siteName,
	setSiteNameError,
	numbersOfEmployees,
	setNumbersOfEmployeesError,
	country,
	setCountryError,
	city,
	setCityError,
	postalCode,
	setPostalCodeError,
	setIsLoading,
	nextMutation,
	updateUserMutation,
	t,
}: {
	siteName: string;
	numbersOfEmployees: string;
	country: string;
	city: string;
	postalCode: string;
	setSiteNameError: React.Dispatch<React.SetStateAction<string>>;
	setNumbersOfEmployeesError: React.Dispatch<React.SetStateAction<string>>;
	setCountryError: React.Dispatch<React.SetStateAction<string>>;
	setCityError: React.Dispatch<React.SetStateAction<string>>;
	setPostalCodeError: React.Dispatch<React.SetStateAction<string>>;

	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	nextMutation: GraphqlMutation<addCompanyDetailsMutation, addCompanyDetailsMutationVariables>;

	updateUserMutation: GraphqlMutationLocal;
	t: TFunction;
}) => {
	const validationResult = validateNextFields({
		siteName,
		setSiteNameError,
		numbersOfEmployees,
		setNumbersOfEmployeesError,
		country,
		setCountryError,
		city,
		setCityError,
		postalCode,
		setPostalCodeError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await nextMutation({
			variables: {
				params: {
					country: country.trim(),
					city: country.trim(),
					postalCode,
					siteName: siteName.trim(),
					employeesNumber: +numbersOfEmployees,
				},
			},
		});

		if (data && data.addCompanyDetails) {
			updateUserMutation({
				variables: {
					token: getToken(),
					isInitialData: true,
				},
			});
			Storage.setItem('user', { token: getToken(), isInitialData: true });
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateNextFields = ({
	siteName,
	setSiteNameError,
	numbersOfEmployees,
	setNumbersOfEmployeesError,
	country,
	setCountryError,
	city,
	setCityError,
	postalCode,
	setPostalCodeError,
	t,
}: {
	siteName: string;
	numbersOfEmployees: string;
	country: string;
	city: string;
	postalCode: string;
	setSiteNameError: React.Dispatch<React.SetStateAction<string>>;
	setNumbersOfEmployeesError: React.Dispatch<React.SetStateAction<string>>;
	setCountryError: React.Dispatch<React.SetStateAction<string>>;
	setCityError: React.Dispatch<React.SetStateAction<string>>;
	setPostalCodeError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;

	if (siteName.trim().length === 0) {
		setSiteNameError(t('Errors.Site name cannot be blank'));
		isSuccess = false;
	} else {
		setSiteNameError('');
	}

	if (numbersOfEmployees.trim().length === 0) {
		setNumbersOfEmployeesError(t('Errors.Number of employees cannot be blank'));
		isSuccess = false;
	} else {
		setNumbersOfEmployeesError('');
	}

	if (country.trim().length === 0) {
		setCountryError(t('Errors.Country cannot be blank'));
		isSuccess = false;
	} else {
		setCountryError('');
	}

	if (city.trim().length === 0) {
		setCityError(t('Errors.City cannot be blank'));
		isSuccess = false;
	} else {
		setCityError('');
	}

	if (postalCode.trim().length === 0) {
		setPostalCodeError(t('Errors.Postal code cannot be blank'));
		isSuccess = false;
	} else {
		setPostalCodeError('');
	}

	return isSuccess;
};
