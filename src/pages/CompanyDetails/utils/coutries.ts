export interface IOptionsCoutries {
	value: string;
	label: string;
}
export const optionsCoutries = [
	{ value: 'Germany', label: 'Germany' },
	{ value: 'UK', label: 'UK' },
	{ value: 'USA', label: 'USA' },
	{ value: 'Russia', label: 'Russia' },
];
