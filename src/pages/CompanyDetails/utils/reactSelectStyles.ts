import { CSSProperties } from 'react';

export const reactSelectStyles = {
	control: (styles: CSSProperties) => ({
		...styles,
		border: '1px solid #a1a9af',
		borderRadius: '9px',
		boxShadow: 'none',
	}),
	singleValue: (styles: CSSProperties) => ({
		...styles,
		fontFamily: 'HelveticaNeue-Medium',
		padding: '0 12px',
		fontWeight: 700,
		fontSize: '22px',
		color: '#243743',
	}),
	option: (styles: CSSProperties) => ({
		...styles,
		fontFamily: 'HelveticaNeue-Medium',
		fontSize: '22px',
		padding: '0 12px',
		color: '#636d73',
	}),
	noOptionsMessage: (styles: CSSProperties) => ({
		...styles,
		fontFamily: 'HelveticaNeue-Medium',
	}),
	placeholder: (styles: CSSProperties) => ({
		...styles,
		fontFamily: 'HelveticaNeue',
		fontStyle: 'normal',
		fontSize: '22px',
		padding: '0 12px',
		color: '#9EA7AC',
	}),
	input: (styles: CSSProperties) => ({
		...styles,
		padding: '0 12px',
		color: '#243743',
		fontSize: '22px',
		height: '50px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'flex-start',
		borderRadius: '9px',
		borderColor: 'rgba(158, 158, 158, 0.07)',
	}),
};
