import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';

import { getCurrentUser } from 'library/graphql/__generated__/getCurrentUser';
import WithLoader from 'library/common/components/WithLoader';
import { USER_DATA_QUERY, UPDATE_USER_DATA } from 'library/graphql/localData/localData';
import { GET_CURRENT_USER } from 'library/graphql/user';
import PendingRegistration from './DashboardFrames/PendingRegistration';
import Header from './DashboardFrames/Header';
import LeftSideBar from './DashboardFrames/LeftSideBar';
import RegistredCompanies from './DashboardFrames/RegistredCompanies';
import UsersDashboard from './DashboardFrames/UsersDashboard';
import Approval from './DashboardFrames/Approval';
import Profile from './DashboardFrames/Profile';

import styles from './dashboard.module.scss';

const Dashboard = () => {
	const { t } = useTranslation();
	const [activeTab, setActiveTab] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const { loading, data: userDataLocal } = useQuery(USER_DATA_QUERY);
	const { loading: userDataLoading, data: userData } = useQuery<getCurrentUser>(GET_CURRENT_USER);
	const [updateUserMutation] = useMutation(UPDATE_USER_DATA);

	useEffect(() => {
		if (userData) {
			updateUserMutation({
				variables: {
					user: {
						...userData.getCurrentUser.user,
						siteName: userData.getCurrentUser.userRegistrationStatus.reservedDomain.name,
					},
				},
			});
			if (userData.getCurrentUser.user.role.id === 1) {
				setActiveTab('registeredCompanies');
			} else if (userData.getCurrentUser.user.role.id === 2) {
				setActiveTab('dashboard');
			}
		}
	}, [userData]);

	return (
		<div className={styles.dashboardContainer}>
			<WithLoader isLoading={userDataLoading}>
				<>
					<LeftSideBar userData={userDataLocal.user} activeTab={activeTab} setActiveTab={setActiveTab} />

					<div className={styles.mainContainer}>
						<Header userData={userDataLocal.user} />
						{activeTab === 'dashboard' && <UsersDashboard />}
						{activeTab === 'pendingApproval' && <div>pendingApproval</div>}
						{activeTab === 'result' && <div>result</div>}
						{activeTab === 'profile' && <Profile />}
						{activeTab === 'registeredCompanies' && <RegistredCompanies userData={userDataLocal.user} />}
						{activeTab === 'pendingRegistration' && <PendingRegistration />}
						{activeTab === 'approval' && <Approval />}
					</div>
				</>
			</WithLoader>
		</div>
	);
};

export default Dashboard;
