import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/react-hooks';
import filter from 'lodash.filter';
import RSC from 'react-scrollbars-custom';

import { GET_COMPANIES_TO_APPROVAL_QUERY } from 'library/graphql/companies';
import {
	getCompaniesToApprovalQuery,
	getCompaniesToApprovalQueryVariables,
	getCompaniesToApprovalQuery_getCompaniesToApproval,
} from 'library/graphql/__generated__/getCompaniesToApprovalQuery';
import WithLoader from 'library/common/components/WithLoader';
import arrowDownGray from 'resources/icons/arrowDownGray.png';
import arrowUpGray from 'resources/icons/arrowUpGray.png';
import ApprovalCompanyData from '../ApprovalCompanyData';

import styles from './approval.module.scss';

const Approval = () => {
	const { t } = useTranslation();
	const [isCollapsed, setIscollapsed] = useState(false);
	const [isApprovalCompanyFrame, setIsApprovalCompanyFrame] = useState(false);
	const [selectedDataId, setSelectedDataId] = useState(0);
	const [selectedUserId, setSelectedUserId] = useState(0);
	const [companiesApproval, setCompaniesApproval] = useState<getCompaniesToApprovalQuery_getCompaniesToApproval[]>([]);
	const [companiesWithErrors, setCompaniesWithError] = useState<getCompaniesToApprovalQuery_getCompaniesToApproval[]>(
		[],
	);

	const { loading: loadindCompleted, data: companiesCompletedQuerry } = useQuery<
		getCompaniesToApprovalQuery,
		getCompaniesToApprovalQueryVariables
	>(GET_COMPANIES_TO_APPROVAL_QUERY, {
		variables: { status: 'approval' },
	});
	const { loading: loadingRemoved, data: companiesRemovedQuerry } = useQuery<
		getCompaniesToApprovalQuery,
		getCompaniesToApprovalQueryVariables
	>(GET_COMPANIES_TO_APPROVAL_QUERY, {
		variables: { status: 'error' },
	});

	useEffect(() => {
		if (companiesCompletedQuerry) {
			setCompaniesApproval(companiesCompletedQuerry.getCompaniesToApproval);
		}
	}, [companiesCompletedQuerry]);

	useEffect(() => {
		if (companiesRemovedQuerry) {
			setCompaniesWithError(companiesRemovedQuerry.getCompaniesToApproval);
		}
	}, [companiesRemovedQuerry]);

	const handleChanges = ({
		response,
		isRemoved,
	}: {
		response: getCompaniesToApprovalQuery_getCompaniesToApproval;
		isRemoved: boolean | undefined;
	}) => {
		if (isRemoved) {
			setCompaniesApproval([...companiesApproval, response]);
			setCompaniesWithError(
				filter(companiesWithErrors, function(o) {
					return o.pendingDataRequestId !== response.pendingDataRequestId;
				}),
			);
		} else {
			setCompaniesWithError([...companiesWithErrors, response]);
			setCompaniesApproval(
				filter(companiesApproval, function(o) {
					return o.pendingDataRequestId !== response.pendingDataRequestId;
				}),
			);
		}
	};

	const handleSelect = (dataId: number, userId: number) => {
		setIsApprovalCompanyFrame(true);
		setSelectedDataId(dataId);
		setSelectedUserId(userId);
	};

	return isApprovalCompanyFrame ? (
		<ApprovalCompanyData
			selectedUserId={selectedUserId}
			handleClose={setIsApprovalCompanyFrame}
			isCompleted={false}
			dataId={selectedDataId}
		/>
	) : (
		<div className={styles.outerContainer}>
			<WithLoader isLoading={loadindCompleted || loadingRemoved}>
				<RSC style={{ height: '100%' }} scrollbarWidth={1}>
					<div className={styles.Container}>
						<div>
							<div className={styles.title}>{t('Dashboard.Approval')}</div>
							<div className={styles.tableContainer}>
								<div className={styles.tableHeader}>
									<div className={styles.headerTextFirst}>{t('Dashboard.Industry Type')}</div>
									<div className={styles.headerText}>{t('Dashboard.Company Name')}</div>
									<div className={styles.headerTextEmail}>{t('Login.Email')}</div>
									<div className={styles.headerText}>{t('Dashboard.Service Group')}</div>
									<div className={styles.headerText}>{t('Dashboard.Service')}</div>
								</div>

								<RSC
									style={
										companiesWithErrors && companiesWithErrors.length === 0
											? { height: '75%' }
											: isCollapsed
											? { height: '70%' }
											: { height: '48%' }
									}
								>
									{companiesApproval &&
										companiesApproval.length > 0 &&
										companiesApproval.map(value => (
											<div key={value.pendingDataRequestId} className={styles.contentWrapper}>
												<div
													onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
													className={styles.contentTextFirst}
												>
													{value.industryName}
												</div>
												<div
													onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
													className={styles.contentText}
												>
													{value.companyName}
												</div>
												<div className={styles.contentTextEmail}>
													<a className={styles.emailHref} href={'mailto:' + value.email}>
														{value.email}
													</a>
												</div>
												<div
													onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
													className={styles.contentText}
												>
													{value.groupName}
												</div>
												<div
													onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
													className={styles.contentText}
												>
													{value.serviceName}
												</div>
											</div>
										))}
								</RSC>

								<div>
									{companiesWithErrors && companiesWithErrors.length > 0 && (
										<div className={styles.foundErrorsHeader}>
											<div className={styles.foundErrorsTitle}>{t('Dashboard.Found Errors')}</div>
											<div className={styles.dashedLine} />
											<div
												onClick={() => {
													setIscollapsed(!isCollapsed);
												}}
												className={styles.icon}
											>
												{isCollapsed ? (
													<img className={styles.arrowLogo} src={arrowDownGray} alt='' />
												) : (
													<img className={styles.arrowLogo} src={arrowUpGray} alt='' />
												)}
											</div>
										</div>
									)}
									<RSC style={{ height: `${window.innerHeight * 0.18}px` }}>
										{!isCollapsed &&
											companiesWithErrors &&
											companiesWithErrors.length > 0 &&
											companiesWithErrors.map(value => (
												<div key={value.pendingDataRequestId} className={styles.contentWrapper}>
													<div
														onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
														className={styles.contentTextFirst}
													>
														{value.industryName}
													</div>
													<div
														onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
														className={styles.contentText}
													>
														{value.companyName}
													</div>
													<div className={styles.contentTextEmail}>
														<a className={styles.emailHref} href={'mailto:' + value.email}>
															{value.email}
														</a>
													</div>
													<div
														onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
														className={styles.contentText}
													>
														{value.groupName}
													</div>
													<div
														onClick={() => handleSelect(value.pendingDataRequestId, value.userId)}
														className={styles.contentText}
													>
														{value.serviceName}
													</div>
												</div>
											))}
									</RSC>
								</div>
							</div>
						</div>
					</div>
				</RSC>
			</WithLoader>
		</div>
	);
};

export default Approval;
