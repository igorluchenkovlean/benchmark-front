import React from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/react-hooks';

import RSC from 'react-scrollbars-custom';
import WithLoader from 'library/common/components/WithLoader';
import crossGray from 'resources/icons/crossGray.svg';
import Button from 'library/common/components/Button/Button';
import { GET_USER_DATA_QUERY } from 'library/graphql/user';
import { getUserDataQuery, getUserDataQueryVariables } from 'library/graphql/__generated__/getUserDataQuery';

import styles from './approvalCompanyData.module.scss';

interface IApprovalCompanyData {
	selectedUserId: number;
	handleClose: React.Dispatch<React.SetStateAction<boolean>>;
	isCompleted: boolean;
	dataId: number;
}
const ApprovalCompanyData = ({ selectedUserId, handleClose, isCompleted, dataId }: IApprovalCompanyData) => {
	const { t } = useTranslation();

	const { loading: userDataLoading, data: userData } = useQuery<getUserDataQuery, getUserDataQueryVariables>(
		GET_USER_DATA_QUERY,
		{
			variables: {
				userId: selectedUserId,
			},
		},
	);

	const ServiceData = {
		serviceName: 'service Name',
		serviceNumber: 'Service Number',
		serviceResults: [
			{
				definition: 'Communicate measures to control and minimize hazards to the safety and health of workers',
				totalCost: {
					current: 226,
					minimum: 170,
					maximum: 212,
					average: 191,
				},
				degreeOfOutsourcing: {
					current: 172,
					minimum: 143,
					maximum: 190,
					average: 166,
				},
				counter: {
					current: 89,
					minimum: 74,
					maximum: 98,
					average: 86,
				},
				denominator: {
					current: 192,
					minimum: 181,
					maximum: 197,
					average: 189,
				},
				KPI: {
					current: 192,
					minimum: 181,
					maximum: 197,
					average: 189,
				},
			},
		],
	};

	return (
		<div className={styles.outerContainer}>
			<WithLoader isLoading={false}>
				<div className={styles.Container}>
					<RSC>
						<div className={styles.titleContainer}>
							<div className={styles.title}>{!isCompleted ? t('Dashboard.Approval') : 'Company Name'}</div>

							<img onClick={() => handleClose(false)} className={styles.cross} src={crossGray} alt='' />
						</div>
						<div className={styles.contentContainer}>
							<WithLoader isLoading={userDataLoading}>
								<div className={styles.userDataContainer}>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Dashboard.Industry')}</div>
										<div className={styles.userText}>{userData?.getUserData.user.company.industry?.name}</div>
									</div>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Dashboard.Company Name')}</div>
										<div className={styles.userText}>{userData?.getUserData.user.company.name}</div>
									</div>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Companie Details.Postal code')}</div>
										<div className={styles.userText}>{userData?.getUserData.user.company.postalCode}</div>
									</div>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Dashboard.Site Name')}</div>
										<div className={styles.userText}>
											{userData?.getUserData.userRegistrationStatus.reservedDomain.name}
										</div>
									</div>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Dashboard.No of Employees')}</div>
										<div className={styles.userText}>{userData?.getUserData.user.company.employeesNumber}</div>
									</div>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Dashboard.Site Manager Name')}</div>
										<div
											className={styles.userText}
										>{`${userData?.getUserData.user.firstName} ${userData?.getUserData.user.lastName}`}</div>
									</div>
									<div className={styles.userDataInfo}>
										<div className={styles.subTitle}>{t('Login.Email')}</div>
										<div className={styles.userText}>{userData?.getUserData.user.email}</div>
									</div>
								</div>
							</WithLoader>
							<hr className={styles.hr} />
							<div>
								<div className={styles.serviceHeaderContainer}>
									<div className={styles.serviceHeader}>{ServiceData.serviceName}</div>
									<div className={styles.serviceHeader}>{ServiceData.serviceNumber}</div>
								</div>

								<div className={styles.serviceGroupHeader}>{t('Dashboard.Service Group Name')}</div>

								<div className={styles.tableHeader}>
									<div className={styles.headerTextFirst}>{t('Dashboard.Definition')}</div>
									<div className={styles.headerText}>
										{t('Dashboard.Total cost of service without cost of projects (in EUR thousand)')}
									</div>
									<div className={styles.headerText}>{t('Dashboard.Degree of outsourcing performance (In %)')}</div>
									<div className={styles.headerText}>{t('Dashboard.ValueKPI Counter')}</div>
									<div className={styles.headerText}>{t('Login.ValueKPI denominator')}</div>
									<div className={styles.headerText}>{t('Dashboard.KPI')}</div>
								</div>

								{ServiceData.serviceResults.map((val, index) => (
									<div key={index + 100} className={styles.contentTableRow}>
										<div className={styles.contentTextFirst}>{val.definition}</div>
										<div className={styles.contentTextWrapper}>
											<div className={styles.contentTextBold}>{val.totalCost.current}</div>
											<div
												className={styles.contentTextGray}
											>{`${val.totalCost.minimum} - ${val.totalCost.maximum}`}</div>
											<div className={styles.contentTextGray}>{val.totalCost.average}</div>
										</div>
										<div className={styles.contentTextWrapper}>
											<div className={styles.contentTextBold}>{val.degreeOfOutsourcing.current}</div>
											<div
												className={styles.contentTextGray}
											>{`${val.degreeOfOutsourcing.minimum} - ${val.degreeOfOutsourcing.maximum}`}</div>
											<div className={styles.contentTextGray}>{val.degreeOfOutsourcing.average}</div>
										</div>
										<div className={styles.contentTextWrapper}>
											<div className={styles.contentTextBold}>{val.counter.current}</div>
											<div className={styles.contentTextGray}>{`${val.counter.minimum} - ${val.counter.maximum}`}</div>
											<div className={styles.contentTextGray}>{val.counter.average}</div>
										</div>
										<div className={styles.contentTextWrapper}>
											<div className={styles.contentTextBold}>{val.denominator.current}</div>
											<div
												className={styles.contentTextGray}
											>{`${val.denominator.minimum} - ${val.denominator.maximum}`}</div>
											<div className={styles.contentTextGray}>{val.denominator.average}</div>
										</div>
										<div className={styles.contentTextWrapper}>
											<div className={styles.contentTextBold}>{val.KPI.current}</div>
											<div className={styles.contentTextGray}>{`${val.KPI.minimum} - ${val.KPI.maximum}`}</div>
											<div className={styles.contentTextGray}>{val.KPI.average}</div>
										</div>
									</div>
								))}
							</div>
						</div>
						{!isCompleted && (
							<div className={styles.buttonsContainer}>
								<Button
									type='red'
									onClick={() => {
										console.log('Export Reports :>> ', 'Export Reports');
									}}
								>
									<div className={styles.buttonInner}>
										<span>{t('Dashboard.Found Errors')}</span>
									</div>
								</Button>
								<Button
									onClick={() => {
										console.log('onClick :>>');
									}}
								>
									<div className={styles.buttonInner}>
										<span>{t('Dashboard.Approve Data')}</span>
									</div>
								</Button>
							</div>
						)}
					</RSC>
				</div>
			</WithLoader>
		</div>
	);
};

export default ApprovalCompanyData;
