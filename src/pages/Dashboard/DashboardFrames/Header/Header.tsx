import React, { useState, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation } from '@apollo/react-hooks';

import Portal from 'library/common/components/Portal';
import { LOGOUT_USER } from 'library/graphql/localData/localData';
import { IUser } from 'core/apollo/initialState';
import arrowDown from 'resources/icons/arrowDown.png';
import { logout } from 'library/utilities/token';
import { useOutsideClick } from 'library/common/hooks/useOutsideClick';

import styles from './header.module.scss';

interface IHeader {
	userData: IUser;
}

const Header = ({ userData }: IHeader) => {
	const { t } = useTranslation();
	const [isPopup, setIsPopup] = useState(false);
	const wrapperRef = useRef<HTMLDivElement | null>(null);
	const [logoutUser] = useMutation(LOGOUT_USER);

	const closeHandler = () => {
		setIsPopup(false);
	};
	useOutsideClick(wrapperRef, closeHandler);

	return (
		<>
			<div className={styles.headerContainer}>
				{userData.role.id === 2 && (
					<div className={styles.userNameContainer}>
						<span className={styles.userNameLabel}>{t('Dashboard.Company Name')}:</span>
						<span className={styles.userName}>{userData.company.name}</span>
						<span className={styles.userNameLabel}>{t('Dashboard.Site Name')}:</span>
						<span className={styles.userName}>{userData.siteName}</span>
					</div>
				)}
				<div className={styles.userNameContainer}>
					<span className={styles.userNameLabel}>{t('Dashboard.Name')}:</span>
					<span className={styles.userName}>{`${userData.firstName} ${userData.lastName}`}</span>
					<div
						className={styles.iconContainer}
						onClick={() => {
							setIsPopup(!isPopup);
						}}
					>
						<img className={styles.arrowDown} src={arrowDown} alt='' />
					</div>
				</div>
			</div>
			{isPopup && (
				<Portal>
					<div
						ref={wrapperRef}
						onClick={() => {
							logout({ logoutUser });
						}}
						className={styles.popup}
					>
						{t('Dashboard.logout')}
					</div>
				</Portal>
			)}
		</>
	);
};

export default Header;
