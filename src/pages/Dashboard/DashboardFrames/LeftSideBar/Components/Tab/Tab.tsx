import React from 'react';
import cn from 'classnames';

import styles from './tab.module.scss';

interface ITab {
	tabLabel: string;
	tabMark: string;
	activeTab: string;
	activeImage: string;
	inactiveImage: string;
	setActiveTab: React.Dispatch<React.SetStateAction<string>>;
}

const Tab = ({ tabLabel, tabMark, activeTab, activeImage, inactiveImage, setActiveTab }: ITab) => (
	<div
		onClick={() => setActiveTab(tabMark)}
		className={cn(styles.tabContainer, { [styles.tabContainerActive]: activeTab === tabMark })}
	>
		<div className={cn(styles.tabImageWrapper, { [styles.tabImageWrapperActive]: activeTab === tabMark })}>
			{activeTab === tabMark ? <img src={activeImage} alt='' /> : <img src={inactiveImage} alt='' />}
		</div>
		<div className={styles.tabLabelWrapper}> {tabLabel}</div>
	</div>
);

export default Tab;
