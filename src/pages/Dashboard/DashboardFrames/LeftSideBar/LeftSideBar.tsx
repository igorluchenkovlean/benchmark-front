import React from 'react';
import { useTranslation } from 'react-i18next';

import { IUser } from 'core/apollo/initialState';
import Tab from 'pages/Dashboard/DashboardFrames/LeftSideBar/Components/Tab';
import logoBig from 'resources/icons/logo-big.svg';
import dashboardBlue from 'resources/icons/dashboardBlue.svg';
import dashboardGray from 'resources/icons/dashboardGray.svg';
import pendingApprovalBlue from 'resources/icons/pendingApprovalBlue.svg';
import pendingApprovalGray from 'resources/icons/pendingApprovalGray.svg';
import profileBlue from 'resources/icons/profileBlue.svg';
import profileGray from 'resources/icons/profileGray.svg';
import resultBlue from 'resources/icons/resultBlue.svg';
import resultGray from 'resources/icons/resultGray.svg';
import starBlue from 'resources/icons/starBlue.svg';
import starGray from 'resources/icons/starGray.svg';

import styles from './leftSideBar.module.scss';

interface ILeftSideBar {
	userData: IUser;
	activeTab: string;
	setActiveTab: React.Dispatch<React.SetStateAction<string>>;
}

const LeftSideBar = ({ userData, activeTab, setActiveTab }: ILeftSideBar) => {
	const { t } = useTranslation();

	return (
		<div className={styles.leftBarContainer}>
			<div className={styles.logoContainer}>
				<img className={styles.logo} src={logoBig} alt='' />
			</div>
			{userData.role.id === 2 ? (
				<div className={styles.tabsContainer}>
					<Tab
						tabLabel={t('Dashboard.Dashboard')}
						tabMark='dashboard'
						activeTab={activeTab}
						activeImage={dashboardBlue}
						inactiveImage={dashboardGray}
						setActiveTab={setActiveTab}
					/>

					<Tab
						tabLabel={t('Dashboard.Pending Approval')}
						tabMark='pendingApproval'
						activeTab={activeTab}
						activeImage={pendingApprovalBlue}
						inactiveImage={pendingApprovalGray}
						setActiveTab={setActiveTab}
					/>

					<Tab
						tabLabel={t('Dashboard.Result')}
						tabMark='result'
						activeTab={activeTab}
						activeImage={resultBlue}
						inactiveImage={resultGray}
						setActiveTab={setActiveTab}
					/>
					<Tab
						tabLabel={t('Dashboard.Profile')}
						tabMark='profile'
						activeTab={activeTab}
						activeImage={profileBlue}
						inactiveImage={profileGray}
						setActiveTab={setActiveTab}
					/>
				</div>
			) : (
				<div className={styles.tabsContainer}>
					<Tab
						tabLabel={t('Dashboard.Registered Companies')}
						tabMark='registeredCompanies'
						activeTab={activeTab}
						activeImage={dashboardBlue}
						inactiveImage={dashboardGray}
						setActiveTab={setActiveTab}
					/>
					<Tab
						tabLabel={t('Dashboard.Pending Registration')}
						tabMark='pendingRegistration'
						activeTab={activeTab}
						activeImage={pendingApprovalBlue}
						inactiveImage={pendingApprovalGray}
						setActiveTab={setActiveTab}
					/>
					<Tab
						tabLabel={t('Dashboard.Approval')}
						tabMark='approval'
						activeTab={activeTab}
						activeImage={starBlue}
						inactiveImage={starGray}
						setActiveTab={setActiveTab}
					/>
				</div>
			)}
		</div>
	);
};

export default LeftSideBar;
