import React from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/react-hooks';
import { format } from 'date-fns';
import RSC from 'react-scrollbars-custom';

import { GET_PENDING_REGISTRATION } from 'library/graphql/companies';
import WithLoader from 'library/common/components/WithLoader';
import { getPeindngUsersRegistrationQuery } from 'library/graphql/__generated__/getPeindngUsersRegistrationQuery';

import styles from './pendingRegistration.module.scss';

const PendingRegistration: React.FC = () => {
	const { t } = useTranslation();

	const { loading, data } = useQuery<getPeindngUsersRegistrationQuery>(GET_PENDING_REGISTRATION, {
		fetchPolicy: 'no-cache',
	});

	return (
		<div className={styles.outerContainer}>
			<WithLoader isLoading={loading}>
				<div className={styles.Container}>
					<div>
						<div className={styles.title}>{t('Dashboard.Companies')}</div>
						<div className={styles.tableContainer}>
							<div className={styles.tableHeader}>
								<div className={styles.headerTextFirst}>{t('Dashboard.Industry Type')}</div>
								<div className={styles.headerText}>{t('Dashboard.Company Name')}</div>
								<div className={styles.headerTextEmail}>{t('Login.Email')}</div>
								<div className={styles.headerText}>{t('Dashboard.Invitation Date')}</div>
							</div>
							<RSC style={{ height: '100%' }}>
								<div className={styles.tableContent}>
									{data?.getPeindngUsersRegistration &&
										data?.getPeindngUsersRegistration.length > 0 &&
										data?.getPeindngUsersRegistration.map(value => (
											<div key={value.id} className={styles.contentWrapper}>
												<div className={styles.contentTextFirst}>{value.industryName}</div>
												<div className={styles.contentText}>{value.companyName}</div>
												<div className={styles.contentTextEmail}>
													<a className={styles.emailHref} href={'mailto:' + value.email}>
														{value.email}
													</a>
												</div>
												<div className={styles.contentText}>{format(new Date(value.createdAt), 'd MMM yyyy')}</div>
											</div>
										))}
								</div>
							</RSC>
						</div>
					</div>
				</div>
			</WithLoader>
		</div>
	);
};

export default PendingRegistration;
