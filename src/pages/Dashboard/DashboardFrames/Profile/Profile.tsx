import React, { useState, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { useTranslation } from 'react-i18next';

import { USER_DATA_QUERY, UPDATE_USER_DATA } from 'library/graphql/localData/localData';
import {
	CHANGE_USER_AND_COMPANY_DATA_MUTATION,
	CHANGE_PASSWORD_MUTATION,
	IS_OLD_PASSWORD_MUTATION,
} from 'library/graphql/companies';
import {
	changeUserAndCompanyDataMutation,
	changeUserAndCompanyDataMutationVariables,
} from 'library/graphql/__generated__/changeUserAndCompanyDataMutation';
import {
	changePasswordMutation,
	changePasswordMutationVariables,
} from 'library/graphql/__generated__/changePasswordMutation';
import {
	isOldPasswordMutation,
	isOldPasswordMutationVariables,
} from 'library/graphql/__generated__/isOldPasswordMutation';
import RSC from 'react-scrollbars-custom';
import WithLoader from 'library/common/components/WithLoader';
import Button from 'library/common/components/Button/Button';
import Input from 'library/common/components/Inputs/Input';
import ErrorMessage from 'library/common/components/ErrorMessage';
import { updatePasswordHandler } from './utils/updatePasswordUtils';
import { submitSaveChangesHandler } from './utils/saveChangesUtils';

import styles from './profile.module.scss';

const Profile = () => {
	const { t } = useTranslation();

	const [firstName, setFirstName] = useState('');
	const [firstNameError, setFirstNameError] = useState('');
	const [lastName, setLastName] = useState('');
	const [lastNameError, setLastNameError] = useState('');
	const [siteName, setSiteName] = useState('');
	const [siteNameError, setSiteNameError] = useState('');
	const [numbersOfEmployees, setNumbersOfEmployees] = useState('');
	const [numbersOfEmployeesError, setNumbersOfEmployeesError] = useState('');
	const [country, setCountry] = useState('');
	const [countryError, setCountryError] = useState('');
	const [city, setCity] = useState('');
	const [cityError, setCityError] = useState('');
	const [postalCode, setPostalCode] = useState('');
	const [postalCodeError, setPostalCodeError] = useState('');
	const [passwordOld, setPasswordOld] = useState('');
	const [passwordOldError, setPasswordOldError] = useState('');
	const [passwordNew, setPasswordNew] = useState('');
	const [passwordNewError, setPasswordNewError] = useState('');
	const [passwordNewSecond, setPasswordNewSecond] = useState('');
	const [passwordNewSecondError, setPasswordNewSecondError] = useState('');
	const [isLoading, setIsLoading] = useState(false);

	const [updateUserMutation] = useMutation(UPDATE_USER_DATA);

	const [changePasswordMutationFunction] = useMutation<changePasswordMutation, changePasswordMutationVariables>(
		CHANGE_PASSWORD_MUTATION,
	);

	const [isOldPassworddMutationFunction] = useMutation<isOldPasswordMutation, isOldPasswordMutationVariables>(
		IS_OLD_PASSWORD_MUTATION,
	);

	const [saveChangesMutation] = useMutation<
		changeUserAndCompanyDataMutation,
		changeUserAndCompanyDataMutationVariables
	>(CHANGE_USER_AND_COMPANY_DATA_MUTATION);
	const { data: userData } = useQuery(USER_DATA_QUERY);

	useEffect(() => {
		if (userData) {
			setFirstName(userData.user.firstName);
			setLastName(userData.user.lastName);
			setSiteName(userData.user.siteName);
			setNumbersOfEmployees(userData.user.company.employeesNumber);
			setCountry(userData.user.company.country);
			setCity(userData.user.company.city);
			setPostalCode(userData.user.company.postalCode);
		}
	}, [userData]);

	const submitUpdatePassword = () =>
		updatePasswordHandler({
			passwordOld,
			setPasswordOldError,
			passwordNew,
			setPasswordNewError,
			passwordNewSecond,
			setPasswordNewSecondError,
			setIsLoading,
			isOldPassworddMutationFunction,
			changePasswordMutationFunction,
			t,
		});

	const submitSaveChanges = () =>
		submitSaveChangesHandler({
			firstName,
			setFirstNameError,
			lastName,
			setLastNameError,
			siteName,
			setSiteNameError,
			numbersOfEmployees,
			setNumbersOfEmployeesError,
			country,
			setCountryError,
			city,
			setCityError,
			postalCode,
			setPostalCodeError,
			setIsLoading,
			saveChangesMutation,
			updateUserMutation,
			userData,
			t,
		});

	const setPostalCodeHandler = (value: string) => {
		if (value.length > 6) return;
		setPostalCode(value);
	};

	const setNumbersOfEmployeesHandler = (value: string) => {
		if (value.length > 10) return;
		setNumbersOfEmployees(value);
	};

	const errors = [
		firstNameError,
		lastNameError,
		siteNameError,
		numbersOfEmployeesError,
		countryError,
		cityError,
		postalCodeError,
	];
	const errorsPassword = [passwordOldError, passwordNewError, passwordNewSecondError];

	return (
		<div className={styles.outerContainer}>
			<WithLoader isLoading={isLoading}>
				<div className={styles.Container}>
					<RSC>
						<div className={styles.title}>{t('Dashboard.Profile')}</div>

						<div className={styles.contentContainer}>
							<div className={styles.userDataContainer}>
								<div className={styles.userDataInfo}>
									<div className={styles.subTitle}>{t('Dashboard.Industry')}</div>
									<div className={styles.userText}>{userData.user.company.industry.name}</div>
								</div>
								<div className={styles.userDataInfo}>
									<div className={styles.subTitle}>{t('Dashboard.Company Name')}</div>
									<div className={styles.userText}>{userData.user.company.name}</div>
								</div>
								<div className={styles.userDataInfo}>
									<div className={styles.subTitle}>{t('Login.Email')}</div>
									<div className={styles.userText}>{userData.user.email}</div>
								</div>
							</div>

							<hr className={styles.hr} />

							<div className={styles.questionsContainer}>
								<div className={styles.subTitle}>{t('Dashboard.Update Information')}</div>
								<div>
									<div className={styles.questionsContainerInput}>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												label={t('Register.First Name')}
												placeholder={t('Register.First Name')}
												value={firstName}
												onChange={setFirstName}
												error={firstNameError}
											/>
										</div>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												label={t('Register.Last Name')}
												placeholder={t('Register.Last Name')}
												value={lastName}
												onChange={setLastName}
												error={lastNameError}
											/>
										</div>
									</div>
									<div className={styles.questionsContainerInput}>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												label={t('Companie Details.Site Name')}
												placeholder={t('Companie Details.Site Name')}
												value={siteName}
												onChange={setSiteName}
												error={siteNameError}
											/>
										</div>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												label={t('Companie Details.Number of employees')}
												placeholder={t('Companie Details.Number of employees')}
												isNumber
												value={numbersOfEmployees}
												onChange={setNumbersOfEmployeesHandler}
												error={numbersOfEmployeesError}
											/>
										</div>
									</div>
									<div className={styles.questionsContainerInput}>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												label={t('Companie Details.Country')}
												placeholder={t('Companie Details.Country')}
												value={country}
												onChange={setCountry}
												error={countryError}
											/>
										</div>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												label={t('Companie Details.City')}
												placeholder={t('Companie Details.City')}
												value={city}
												onChange={setCity}
												error={cityError}
											/>
										</div>
										<div className={styles.inputWrapper}>
											<Input
												isServiceQuestion
												isNumber
												label={t('Companie Details.Postal code')}
												placeholder={t('Companie Details.Postal code')}
												value={postalCode}
												onChange={setPostalCodeHandler}
												error={postalCodeError}
											/>
										</div>
									</div>
								</div>
							</div>
							<div className={styles.buttonsContainer}>
								<div>
									<ErrorMessage errors={errors} />
									<Button type='dark' onClick={submitSaveChanges}>
										{t('Dashboard.Save Changes')}
									</Button>
								</div>
							</div>

							<hr className={styles.hr} />

							<div className={styles.questionsContainer}>
								<div className={styles.subTitle}>{t('Reset.Update Password')}</div>
								<div className={styles.questionsContainerInput}>
									<div className={styles.inputWrapper}>
										<Input
											label={t('Dashboard.Enter old password')}
											placeholder={t('Dashboard.Enter your old password')}
											value={passwordOld}
											type='password'
											onChange={setPasswordOld}
											error={passwordOldError}
										/>
									</div>
									<div className={styles.inputWrapper}>
										<Input
											label={t('Dashboard.Enter new password')}
											placeholder={t('Dashboard.Enter your new password')}
											value={passwordNew}
											type='password'
											onChange={setPasswordNew}
											error={passwordNewError}
										/>
									</div>
									<div className={styles.inputWrapper}>
										<Input
											label={t('Dashboard.Re- enter new password')}
											placeholder={t('Dashboard.Re-enter your new password')}
											value={passwordNewSecond}
											type='password'
											onChange={setPasswordNewSecond}
											error={passwordNewSecondError}
										/>
									</div>
								</div>
							</div>
							<div className={styles.buttonsContainer}>
								<div>
									<ErrorMessage errors={errorsPassword} />
									<Button type='dark' onClick={submitUpdatePassword}>
										{t('Reset.Update Password')}
									</Button>
								</div>
							</div>
						</div>
					</RSC>
				</div>
			</WithLoader>
		</div>
	);
};

export default Profile;
