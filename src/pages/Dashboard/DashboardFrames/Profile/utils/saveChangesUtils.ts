import { TFunction } from 'i18next';

import { GraphqlMutation, GraphqlMutationLocal } from 'library/common/types/graphql';
import {
	changeUserAndCompanyDataMutation,
	changeUserAndCompanyDataMutationVariables,
} from 'library/graphql/__generated__/changeUserAndCompanyDataMutation';
import { IUser } from 'core/apollo/initialState';

export const submitSaveChangesHandler = async ({
	firstName,
	setFirstNameError,
	lastName,
	setLastNameError,
	siteName,
	setSiteNameError,
	numbersOfEmployees,
	setNumbersOfEmployeesError,
	country,
	setCountryError,
	city,
	setCityError,
	postalCode,
	setPostalCodeError,
	setIsLoading,
	saveChangesMutation,
	updateUserMutation,
	userData,
	t,
}: {
	firstName: string;
	setFirstNameError: React.Dispatch<React.SetStateAction<string>>;
	lastName: string;
	setLastNameError: React.Dispatch<React.SetStateAction<string>>;
	siteName: string;
	numbersOfEmployees: string;
	country: string;
	city: string;
	postalCode: string;
	setSiteNameError: React.Dispatch<React.SetStateAction<string>>;
	setNumbersOfEmployeesError: React.Dispatch<React.SetStateAction<string>>;
	setCountryError: React.Dispatch<React.SetStateAction<string>>;
	setCityError: React.Dispatch<React.SetStateAction<string>>;
	setPostalCodeError: React.Dispatch<React.SetStateAction<string>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	saveChangesMutation: GraphqlMutation<changeUserAndCompanyDataMutation, changeUserAndCompanyDataMutationVariables>;
	updateUserMutation: GraphqlMutationLocal;
	userData: { user: IUser };
	t: TFunction;
}) => {
	const validationResult = validateSaveChanges({
		firstName,
		setFirstNameError,
		lastName,
		setLastNameError,
		siteName,
		setSiteNameError,
		numbersOfEmployees,
		setNumbersOfEmployeesError,
		country,
		setCountryError,
		city,
		setCityError,
		postalCode,
		setPostalCodeError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await saveChangesMutation({
			variables: {
				params: {
					firstName: firstName.trim(),
					lastName: lastName.trim(),
					country: country.trim(),
					city: city.trim(),
					postalCode,
					siteName: siteName.trim(),
					employeesNumber: +numbersOfEmployees,
				},
			},
		});

		if (data && data.changeUserAndCompanyData) {
			updateUserMutation({
				variables: {
					user: {
						...userData.user,
						siteName: siteName.trim(),
						firstName: firstName.trim(),
						lastName: lastName.trim(),
						company: {
							...userData.user.company,
							country: country.trim(),
							city: country.trim(),
							postalCode,
							employeesNumber: +numbersOfEmployees,
						},
					},
				},
			});
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateSaveChanges = ({
	firstName,
	setFirstNameError,
	lastName,
	setLastNameError,
	siteName,
	setSiteNameError,
	numbersOfEmployees,
	setNumbersOfEmployeesError,
	country,
	setCountryError,
	city,
	setCityError,
	postalCode,
	setPostalCodeError,
	t,
}: {
	firstName: string;
	lastName: string;
	siteName: string;
	numbersOfEmployees: string;
	country: string;
	city: string;
	postalCode: string;
	setLastNameError: React.Dispatch<React.SetStateAction<string>>;
	setFirstNameError: React.Dispatch<React.SetStateAction<string>>;
	setSiteNameError: React.Dispatch<React.SetStateAction<string>>;
	setNumbersOfEmployeesError: React.Dispatch<React.SetStateAction<string>>;
	setCountryError: React.Dispatch<React.SetStateAction<string>>;
	setCityError: React.Dispatch<React.SetStateAction<string>>;
	setPostalCodeError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;

	if (firstName.trim().length === 0) {
		setFirstNameError(t('Errors.First name cannot be blank'));
		isSuccess = false;
	} else {
		setFirstNameError('');
	}

	if (lastName.trim().length === 0) {
		setLastNameError(t('Errors.Last name cannot be blank'));
		isSuccess = false;
	} else {
		setLastNameError('');
	}

	if (siteName.trim().length === 0) {
		setSiteNameError(t('Errors.Site name cannot be blank'));
		isSuccess = false;
	} else {
		setSiteNameError('');
	}

	if (numbersOfEmployees.length === 0) {
		setNumbersOfEmployeesError(t('Errors.Number of employees cannot be blank'));
		isSuccess = false;
	} else {
		setNumbersOfEmployeesError('');
	}

	if (country.trim().length === 0) {
		setCountryError(t('Errors.Country cannot be blank'));
		isSuccess = false;
	} else {
		setCountryError('');
	}

	if (city.trim().length === 0) {
		setCityError(t('Errors.City cannot be blank'));
		isSuccess = false;
	} else {
		setCityError('');
	}

	if (postalCode.trim().length === 0) {
		setPostalCodeError(t('Errors.Postal code cannot be blank'));
		isSuccess = false;
	} else {
		setPostalCodeError('');
	}

	return isSuccess;
};
