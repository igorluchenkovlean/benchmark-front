import { TFunction } from 'i18next';

import { GraphqlMutation } from 'library/common/types/graphql';
import {
	changePasswordMutation,
	changePasswordMutationVariables,
} from 'library/graphql/__generated__/changePasswordMutation';
import {
	isOldPasswordMutation,
	isOldPasswordMutationVariables,
} from 'library/graphql/__generated__/isOldPasswordMutation';

export const updatePasswordHandler = async ({
	passwordOld,
	setPasswordOldError,
	passwordNew,
	setPasswordNewError,
	passwordNewSecond,
	setPasswordNewSecondError,
	setIsLoading,
	isOldPassworddMutationFunction,
	changePasswordMutationFunction,
	t,
}: {
	passwordOld: string;
	setPasswordOldError: React.Dispatch<React.SetStateAction<string>>;
	passwordNew: string;
	setPasswordNewError: React.Dispatch<React.SetStateAction<string>>;
	passwordNewSecond: string;
	setPasswordNewSecondError: React.Dispatch<React.SetStateAction<string>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	isOldPassworddMutationFunction: GraphqlMutation<isOldPasswordMutation, isOldPasswordMutationVariables>;
	changePasswordMutationFunction: GraphqlMutation<changePasswordMutation, changePasswordMutationVariables>;
	t: TFunction;
}) => {
	const validationResult = validateUpdatePassword({
		passwordOld,
		setPasswordOldError,
		passwordNew,
		setPasswordNewError,
		passwordNewSecond,
		setPasswordNewSecondError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data: oldPassword } = await isOldPassworddMutationFunction({
			variables: {
				password: passwordOld.trim(),
			},
		});
		if (oldPassword?.isOldPassword) {
			const { data } = await changePasswordMutationFunction({
				variables: {
					password: passwordNew.trim(),
				},
			});
		} else {
			setPasswordOldError(t('Errors.Old password is wrong'));
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateUpdatePassword = ({
	passwordOld,
	setPasswordOldError,
	passwordNew,
	setPasswordNewError,
	passwordNewSecond,
	setPasswordNewSecondError,
	t,
}: {
	passwordOld: string;
	passwordNew: string;
	passwordNewSecond: string;
	setPasswordOldError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordNewError: React.Dispatch<React.SetStateAction<string>>;
	setPasswordNewSecondError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;

	if (passwordOld.trim().length === 0) {
		setPasswordOldError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else {
		setPasswordOldError('');
	}

	if (passwordNew.trim().length === 0) {
		setPasswordNewError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else {
		setPasswordNewError('');
	}

	if (passwordNewSecond.trim().length === 0) {
		setPasswordNewSecondError(t('Errors.Password cannot be blank'));
		isSuccess = false;
	} else if (passwordNewSecond.trim() !== passwordNew.trim()) {
		setPasswordNewSecondError(t('Errors.Passwords must match'));
		isSuccess = false;
	} else {
		setPasswordNewSecondError('');
	}

	return isSuccess;
};
