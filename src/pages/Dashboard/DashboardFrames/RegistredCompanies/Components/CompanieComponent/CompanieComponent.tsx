import React, { useState, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation } from '@apollo/react-hooks';

import { CHANGE_COMPANIE_STATUS_MUTATION } from 'library/graphql/companies';
import { getRegisteredCompanies_getRegisteredCompanies } from 'library/graphql/__generated__/getRegisteredCompanies';
import Portal from 'library/common/components/Portal';
import { getCoordinates } from './utils/getCoordinates';
import grayDots from 'resources/icons/grayDots.png';
import { useOutsideClick } from 'library/common/hooks/useOutsideClick';

import styles from './companieComponent.module.scss';

interface ICompanieComponent {
	companyName: string;
	email: string;
	employeesNumber: number;
	id: number;
	userId: number;
	industryName: string;
	postalCode: string;
	siteName: string;
	isRemoved?: boolean;
	handleChanges: ({
		response,
		isRemoved,
	}: {
		response: getRegisteredCompanies_getRegisteredCompanies;
		isRemoved: boolean | undefined;
	}) => void;
	handleSelect: (id: number, userId: number) => void;
}

const CompanieComponent = ({
	companyName,
	email,
	employeesNumber,
	id,
	userId,
	industryName,
	postalCode,
	siteName,
	isRemoved,
	handleChanges,
	handleSelect,
}: ICompanieComponent) => {
	const { t } = useTranslation();
	const wrapperRef = useRef<HTMLDivElement | null>(null);
	const buttonRef = useRef<HTMLDivElement | null>(null);
	const [isPopup, setIsPopup] = useState(false);
	const [top, setTop] = useState<number | null>(null);
	const [left, setLeft] = useState<number | null>(null);

	const [changeStatusMutation] = useMutation(CHANGE_COMPANIE_STATUS_MUTATION);
	const closeHandler = () => {
		setIsPopup(false);
	};

	useEffect(() => {
		setTop(getCoordinates(buttonRef).top);
		setLeft(getCoordinates(buttonRef).left);
	});

	const submitChanges = async () => {
		const data = await changeStatusMutation({
			variables: {
				params: {
					userId: id,
					status: `${isRemoved ? 'completed' : 'removed'}`,
				},
			},
		});

		const response = data.data.changeStatusCompany;
		handleChanges({ response, isRemoved });
		setIsPopup(false);
	};
	useOutsideClick(wrapperRef, closeHandler);

	return (
		<>
			<div className={styles.container}>
				<div onClick={() => handleSelect(id, userId)} className={styles.textFirst}>
					{industryName}
				</div>
				<div onClick={() => handleSelect(id, userId)} className={styles.text}>
					{companyName}
				</div>
				userId
				<div onClick={() => handleSelect(id, userId)} className={styles.number}>
					{postalCode}
				</div>
				<div onClick={() => handleSelect(id, userId)} className={styles.text}>
					{siteName}
				</div>
				<div className={styles.email}>
					<a className={styles.emailHref} href={'mailto:' + email}>
						{email}
					</a>
				</div>
				<div onClick={() => handleSelect(id, userId)} className={styles.number}>{`${employeesNumber}`}</div>
				<div
					ref={buttonRef}
					onClick={() => {
						setIsPopup(true);
					}}
					className={styles.icon}
				>
					<img className={styles.imgIcon} src={grayDots} alt='' />
				</div>
			</div>
			{isPopup && (
				<Portal>
					<div
						ref={wrapperRef}
						onClick={submitChanges}
						style={{
							top: `${top}px`,
							left: `${left}px`,
						}}
						className={styles.popup}
					>
						{isRemoved ? (
							<span className={styles.giveAccess}>{t('Dashboard.Give Access')}</span>
						) : (
							<span className={styles.remove}>{t('Dashboard.Remove')}</span>
						)}
					</div>
				</Portal>
			)}
		</>
	);
};

export default CompanieComponent;
