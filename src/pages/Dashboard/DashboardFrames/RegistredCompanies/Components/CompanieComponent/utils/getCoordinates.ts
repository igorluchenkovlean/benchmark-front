export const getCoordinates = (content: React.RefObject<HTMLDivElement>) => {
	const rect = content.current && content.current.getBoundingClientRect();

	const top = getTopCoordinate(content, rect);
	const left = getLeftCoordinate(content, rect);

	return {
		top,
		left,
	};
};

export const getTopCoordinate = (content: React.RefObject<HTMLDivElement>, rect: DOMRect | null) =>
	content.current && rect && rect.top + window.pageYOffset + content.current.offsetHeight;

export const getLeftCoordinate = (content: React.RefObject<HTMLDivElement>, rect: DOMRect | null) =>
	content.current && rect && rect.left + content.current.offsetWidth + window.pageXOffset - 130;
