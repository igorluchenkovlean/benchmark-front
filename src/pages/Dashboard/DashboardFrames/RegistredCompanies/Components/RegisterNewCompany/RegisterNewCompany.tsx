import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation, useQuery } from '@apollo/react-hooks';
import Select from 'react-select';
import Modal from 'react-modal';

import { ADD_COMPANY_MUTATION, GET_INDUSTRIES_QUERY } from 'library/graphql/companies';
import { addCompanyMutation, addCompanyMutationVariables } from 'library/graphql/__generated__/addCompanyMutation';
import { getIndustriesQuery } from 'library/graphql/__generated__/getIndustriesQuery';
import plus from 'resources/icons/plus.png';
import arrowLeft from 'resources/icons/arrowLeft.svg';
import warning from 'resources/icons/warning.svg';
import Button from 'library/common/components/Button/Button';
import Input from 'library/common/components/Inputs/Input';
import WithLoader from 'library/common/components/WithLoader';
import ErrorMessage from 'library/common/components/ErrorMessage';
import { submitRegisterNewCompanyHandler } from './utils/registerNewCompanyUtils';
import { reactSelectStyles } from './utils/reactSelectStyles';

import styles from './registerNewCompany.module.scss';

export interface IOptionsIndustries {
	value: string;
	label: string;
}
interface IRegisterNewCompany {
	setIsNewCompanyFrame: React.Dispatch<React.SetStateAction<boolean>>;
}

const RegisterNewCompany = ({ setIsNewCompanyFrame }: IRegisterNewCompany) => {
	const { t } = useTranslation();

	const [optionsIndustries, setOptionsIndustries] = useState<IOptionsIndustries[]>([]);
	const [industrie, setIndustrie] = useState('');
	const [industrieError, setIndustrieError] = useState('');
	const [companyName, setCompanyName] = useState('');
	const [companyNameError, setCompanyNameError] = useState('');
	const [email, setEmail] = useState('');
	const [emailError, setEmailError] = useState('');
	const [isPopup, setIsPopup] = useState(false);
	const [isLoading, setIsLoading] = useState(false);

	const [addCompanyMutationFunction] = useMutation<addCompanyMutation, addCompanyMutationVariables>(
		ADD_COMPANY_MUTATION,
	);

	const { loading: industriesLoading, data: industries } = useQuery<getIndustriesQuery>(GET_INDUSTRIES_QUERY);

	useEffect(() => {
		if (industries) {
			const arrayOfIndustries = industries.getIndustries.map(val => ({
				value: val.name,
				label: val.name,
			}));
			setOptionsIndustries(arrayOfIndustries);
		}
	}, [industries]);
	const submitAddNewCompany = () =>
		submitRegisterNewCompanyHandler({
			email,
			setEmailError,
			industrie,
			setIndustrieError,
			companyName,
			setCompanyNameError,
			setIsPopup,
			setIsLoading,
			addCompanyMutationFunction,
			t,
		});

	const handleChangeIndustrieSelect = (e: any) => {
		if (industrieError) {
			setIndustrieError('');
		}
		setIndustrie(e.value);
	};

	const errors = [emailError, industrieError, companyNameError];

	return (
		<>
			<Modal
				isOpen={isPopup}
				className={styles.modal}
				overlayClassName={styles.modalOverlay}
				onRequestClose={() => {
					setIsPopup(false);
					setIndustrie('');
					setCompanyName('');
					setEmail('');
				}}
			>
				<div className={styles.popupWrapper}>
					<span className={styles.popupText}>{t('Dashboard.We have shared invitation link to')}</span>
					<div>
						<span className={styles.popupTextBold}>{email}</span>
						<span className={styles.popupText}>{t('Dashboard.for')}</span>
						<span className={styles.popupTextBold}>{companyName}</span>
					</div>
				</div>
			</Modal>
			<div className={styles.outerContainer}>
				<WithLoader isLoading={isLoading}>
					<div className={styles.container}>
						<div className={styles.contentContainer}>
							<div
								className={styles.arrowContainer}
								onClick={() => {
									setIsNewCompanyFrame(false);
								}}
							>
								<img className={styles.arrow} src={arrowLeft} alt='' />
							</div>
							<div className={styles.title}>{t('Dashboard.Add new company')}</div>

							<div className={styles.selectIndustrie}>
								<div className={styles.labelIndustrie}>{t('Dashboard.Industry')}</div>
								<div className={styles.inputIndustrieContainer}>
									<Select
										value={
											optionsIndustries &&
											optionsIndustries.filter((obj: IOptionsIndustries) => obj.value === industrie)
										}
										options={optionsIndustries}
										styles={reactSelectStyles}
										placeholder={t('Dashboard.Select industry type')}
										noOptionsMessage={() => t('Errors.No Such Industry')}
										onChange={handleChangeIndustrieSelect}
									/>
									{industrieError && <img className={styles.warning} src={warning} alt='' />}
								</div>
							</div>
							<div className={styles.inputsContainer}>
								<div className={styles.inputWrapper}>
									<Input
										label={t('Dashboard.Company Name')}
										placeholder={t('Dashboard.Enter company name')}
										value={companyName}
										onChange={setCompanyName}
										error={companyNameError}
										isLight
									/>
								</div>
								<div className={styles.inputWrapper}>
									<Input
										label={t('Login.Email')}
										placeholder={t('Dashboard.Enter email address to send invite')}
										value={email}
										onChange={setEmail}
										error={emailError}
										isLight
									/>
								</div>
							</div>
						</div>
						<div className={styles.buttonContainer}>
							<div className={styles.buttonAndErrorContainer}>
								<ErrorMessage errors={errors} />
								<Button onClick={submitAddNewCompany}>
									<div className={styles.buttonInner}>
										<img className={styles.buttonLogo} src={plus} alt='' />
										<span>{t('Dashboard.New Company')}</span>
									</div>
								</Button>
							</div>
						</div>
					</div>
				</WithLoader>
			</div>
		</>
	);
};

export default RegisterNewCompany;
