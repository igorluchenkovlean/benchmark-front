import { TFunction } from 'i18next';

import { validateEmail } from 'library/utilities/validation';
import { GraphqlMutation, UserLanguage } from 'library/common/types/graphql';
import { addCompanyMutation, addCompanyMutationVariables } from 'library/graphql/__generated__/addCompanyMutation';

export const submitRegisterNewCompanyHandler = async ({
	email,
	setEmailError,
	industrie,
	setIndustrieError,
	companyName,
	setCompanyNameError,
	setIsPopup,
	setIsLoading,
	addCompanyMutationFunction,
	t,
}: {
	email: string;
	industrie: string;
	companyName: string;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	setIndustrieError: React.Dispatch<React.SetStateAction<string>>;
	setCompanyNameError: React.Dispatch<React.SetStateAction<string>>;
	setIsPopup: React.Dispatch<React.SetStateAction<boolean>>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	addCompanyMutationFunction: GraphqlMutation<addCompanyMutation, addCompanyMutationVariables>;
	t: TFunction;
}) => {
	const validationResult = validateRegisterNewCompany({
		email,
		setEmailError,
		industrie,
		setIndustrieError,
		companyName,
		setCompanyNameError,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await addCompanyMutationFunction({
			variables: {
				params: {
					email: email.trim(),
					industryName: industrie.trim(),
					companyName: companyName.trim(),
					language: UserLanguage.en,
				},
			},
		});

		if (data && data.addCompany) {
			setIsPopup(true);
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
		if (graphQLErrors.length && graphQLErrors[0].message === 'Company already exists') {
			setCompanyNameError(t('Errors.Company already exists'));
		} else if (graphQLErrors.length && graphQLErrors[0].message === 'User already exists') {
			setEmailError(t('Errors.User already exists'));
		}
	} finally {
		setIsLoading(false);
	}
};

export const validateRegisterNewCompany = ({
	email,
	setEmailError,
	industrie,
	setIndustrieError,
	companyName,
	setCompanyNameError,
	t,
}: {
	email: string;
	industrie: string;
	companyName: string;
	setEmailError: React.Dispatch<React.SetStateAction<string>>;
	setIndustrieError: React.Dispatch<React.SetStateAction<string>>;
	setCompanyNameError: React.Dispatch<React.SetStateAction<string>>;
	t: TFunction;
}) => {
	let isSuccess = true;
	if (email.trim().length === 0) {
		setEmailError(t('Errors.Email cannot be blank'));
		isSuccess = false;
	} else if (!validateEmail(email)) {
		isSuccess = false;
		setEmailError(t('Errors.Email is incorrect'));
	} else {
		setEmailError('');
	}

	if (industrie.trim().length === 0) {
		setIndustrieError(t('Errors.Industrie cannot be blank'));
		isSuccess = false;
	} else {
		setIndustrieError('');
	}

	if (companyName.trim().length === 0) {
		setCompanyNameError(t('Errors.Company Name cannot be blank'));
		isSuccess = false;
	} else {
		setCompanyNameError('');
	}

	return isSuccess;
};
