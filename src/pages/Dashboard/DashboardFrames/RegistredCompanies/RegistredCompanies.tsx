import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/react-hooks';
import filter from 'lodash.filter';
import RSC from 'react-scrollbars-custom';

import { GET_REGISTERED_COMPANIES_QUERY } from 'library/graphql/companies';
import {
	getRegisteredCompanies,
	getRegisteredCompanies_getRegisteredCompanies,
} from 'library/graphql/__generated__/getRegisteredCompanies';
import WithLoader from 'library/common/components/WithLoader';
import { IUser } from 'core/apollo/initialState';
import Button from 'library/common/components/Button/Button';
import download from 'resources/icons/download.png';
import plus from 'resources/icons/plus.png';
import arrowDownGray from 'resources/icons/arrowDownGray.png';
import arrowUpGray from 'resources/icons/arrowUpGray.png';
import RegisterNewCompany from './Components/RegisterNewCompany';
import CompanieComponent from './Components/CompanieComponent';

import styles from './registredCompanies.module.scss';
import ApprovalCompanyData from '../ApprovalCompanyData';

interface IHeader {
	userData: IUser;
}

const RegistredCompanies = ({ userData }: IHeader) => {
	const { t } = useTranslation();
	const [isCollapsed, setIscollapsed] = useState(false);
	const [isApprovalCompanyFrame, setIsApprovalCompanyFrame] = useState(false);
	const [selectedDataId, setSelectedDataId] = useState(0);
	const [selectedUserId, setSelectedUserId] = useState(0);
	const [isNewCompanyFrame, setIsNewCompanyFrame] = useState(false);
	const [companiesCompleted, setCompaniesCompleted] = useState<getRegisteredCompanies_getRegisteredCompanies[]>([]);
	const [companiesRemoved, setCompaniesRemoved] = useState<getRegisteredCompanies_getRegisteredCompanies[]>([]);

	const { loading: loadindCompleted, data: companiesCompletedQuerry } = useQuery<getRegisteredCompanies>(
		GET_REGISTERED_COMPANIES_QUERY,
		{
			variables: { params: { status: 'completed' } },
		},
	);
	const { loading: loadingRemoved, data: companiesRemovedQuerry } = useQuery<getRegisteredCompanies>(
		GET_REGISTERED_COMPANIES_QUERY,
		{
			variables: { params: { status: 'removed' } },
		},
	);

	useEffect(() => {
		if (companiesCompletedQuerry) {
			setCompaniesCompleted(companiesCompletedQuerry.getRegisteredCompanies);
		}
	}, [companiesCompletedQuerry]);

	useEffect(() => {
		if (companiesRemovedQuerry) {
			setCompaniesRemoved(companiesRemovedQuerry.getRegisteredCompanies);
		}
	}, [companiesRemovedQuerry]);

	const handleChanges = ({
		response,
		isRemoved,
	}: {
		response: getRegisteredCompanies_getRegisteredCompanies;
		isRemoved: boolean | undefined;
	}) => {
		if (isRemoved) {
			setCompaniesCompleted([...companiesCompleted, response]);
			setCompaniesRemoved(
				filter(companiesRemoved, function(o) {
					return o.id !== response.id;
				}),
			);
		} else {
			setCompaniesRemoved([...companiesRemoved, response]);
			setCompaniesCompleted(
				filter(companiesCompleted, function(o) {
					return o.id !== response.id;
				}),
			);
		}
	};

	const handleSelect = (dataId: number, userId: number) => {
		setIsApprovalCompanyFrame(true);
		setSelectedDataId(dataId);
		setSelectedUserId(userId);
	};

	return isNewCompanyFrame ? (
		<RegisterNewCompany setIsNewCompanyFrame={setIsNewCompanyFrame} />
	) : isApprovalCompanyFrame ? (
		<ApprovalCompanyData
			selectedUserId={selectedUserId}
			handleClose={setIsApprovalCompanyFrame}
			isCompleted={true}
			dataId={selectedDataId}
		/>
	) : (
		<div className={styles.outerContainer}>
			<WithLoader isLoading={loadindCompleted || loadingRemoved}>
				<RSC style={{ height: '100%' }}>
					<div className={styles.registredCompaniesContainer}>
						<div>
							<div className={styles.title}>{t('Dashboard.Companies')}</div>
							<div className={styles.tableContainer}>
								<div className={styles.tableHeader}>
									<div className={styles.headerTextFirst}>{t('Dashboard.Industry Type')}</div>
									<div className={styles.headerText}>{t('Dashboard.Company Name')}</div>
									<div className={styles.headerTextNumber}>{t('Companie Details.Postal code')}</div>
									<div className={styles.headerText}>{t('Dashboard.Site Name')}</div>
									<div className={styles.headerTextEmail}>{t('Login.Email')}</div>
									<div className={styles.headerTextNumber}>{t('Dashboard.No of Employees')}</div>
								</div>

								<RSC
									style={
										companiesRemoved && companiesRemoved.length === 0
											? { height: '75%' }
											: isCollapsed
											? { height: '70%' }
											: { height: '48%' }
									}
								>
									{companiesCompleted &&
										companiesCompleted.length > 0 &&
										companiesCompleted.map(value => (
											<CompanieComponent
												key={value.id}
												companyName={value.companyName}
												email={value.email}
												employeesNumber={value.employeesNumber}
												id={value.id}
												userId={value.userId}
												industryName={value.industryName}
												postalCode={value.postalCode}
												siteName={value.siteName}
												handleChanges={handleChanges}
												isRemoved={false}
												handleSelect={handleSelect}
											/>
										))}
								</RSC>

								<div className={styles.accessRemoved}>
									{companiesRemoved && companiesRemoved.length > 0 && (
										<div className={styles.accessRemovedHeader}>
											<div className={styles.accessRemovedTitle}>{t('Dashboard.Access removed')}</div>
											<div className={styles.dashedLine} />
											<div
												onClick={() => {
													setIscollapsed(!isCollapsed);
												}}
												className={styles.icon}
											>
												{isCollapsed ? (
													<img className={styles.arrowLogo} src={arrowDownGray} alt='' />
												) : (
													<img className={styles.arrowLogo} src={arrowUpGray} alt='' />
												)}
											</div>
										</div>
									)}

									<RSC style={{ height: `${window.innerHeight * 0.18}px` }}>
										{!isCollapsed &&
											companiesRemoved &&
											companiesRemoved.length > 0 &&
											companiesRemoved.map(value => (
												<CompanieComponent
													key={value.id}
													companyName={value.companyName}
													email={value.email}
													employeesNumber={value.employeesNumber}
													id={value.id}
													userId={value.userId}
													industryName={value.industryName}
													postalCode={value.postalCode}
													siteName={value.siteName}
													isRemoved
													handleChanges={handleChanges}
													handleSelect={handleSelect}
												/>
											))}
									</RSC>
								</div>
							</div>
						</div>
						<div className={styles.buttonsContainer}>
							<Button
								type='light'
								onClick={() => {
									console.log('Export Reports :>> ', 'Export Reports');
								}}
							>
								<div className={styles.buttonInner}>
									<img className={styles.buttonLogo} src={download} alt='' />
									<span>{t('Dashboard.Export Reports')}</span>
								</div>
							</Button>
							<Button
								onClick={() => {
									setIsNewCompanyFrame(true);
								}}
							>
								<div className={styles.buttonInner}>
									<img className={styles.buttonLogo} src={plus} alt='' />
									<span>{t('Dashboard.New Company')}</span>
								</div>
							</Button>
						</div>
					</div>
				</RSC>
			</WithLoader>
		</div>
	);
};

export default RegistredCompanies;
