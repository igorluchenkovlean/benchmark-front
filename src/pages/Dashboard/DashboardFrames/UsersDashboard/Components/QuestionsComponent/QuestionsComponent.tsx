import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Modal from 'react-modal';

import { GET_SERVICES_QUERY, UPDATE_SERVICE_VALUE_MUTATION } from 'library/graphql/companies';
import { getServiceQuery, getServiceQueryVariables } from 'library/graphql/__generated__/getServiceQuery';
import {
	updateServiceValueMutation,
	updateServiceValueMutationVariables,
} from 'library/graphql/__generated__/updateServiceValueMutation';
import ErrorMessage from 'library/common/components/ErrorMessage';
import WithLoader from 'library/common/components/WithLoader';
import Button from 'library/common/components/Button/Button';
import Input from 'library/common/components/Inputs/Input';
import arrowLeft from 'resources/icons/arrowLeft.svg';
import { submitValuesHandler } from './utils/questionsComponentUtils';
import { DashboardFrame } from '../../UsersDashboard';

import styles from './questionsComponent.module.scss';

interface IQuestionsComponent {
	selectedGroupName: string;
	selectedServiceName: string;
	selectedServiceId: number;
	setCurrentFrame: React.Dispatch<React.SetStateAction<DashboardFrame>>;
}

const QuestionsComponent = ({
	selectedGroupName,
	selectedServiceName,
	selectedServiceId,
	setCurrentFrame,
}: IQuestionsComponent) => {
	const { t } = useTranslation();

	const [question1, setQuestion1] = useState<string>('');
	const [question1Error, setQuestion1Error] = useState('');

	const [question2, setQuestion2] = useState<string>('');
	const [question2Error, setQuestion2Error] = useState('');

	const [question3, setQuestion3] = useState<string>('');
	const [question3Error, setQuestion3Error] = useState('');

	const [question4, setQuestion4] = useState<string>('');
	const [question4Error, setQuestion4Error] = useState('');

	const [question5, setQuestion5] = useState<string>('');
	const [question5Error, setQuestion5Error] = useState('');

	const [isLoading, setIsLoading] = useState(false);
	const [submitModalIsOpen, setSubmitModalIsOpen] = useState(false);

	const arrayOfQuestions = [
		{
			value: question1,
			setValue: setQuestion1,
			error: question1Error,
			setError: setQuestion1Error,
		},
		{
			value: question2,
			setValue: setQuestion2,
			error: question2Error,
			setError: setQuestion2Error,
		},
		{
			value: question3,
			setValue: setQuestion3,
			error: question3Error,
			setError: setQuestion3Error,
		},
		{
			value: question4,
			setValue: setQuestion4,
			error: question4Error,
			setError: setQuestion4Error,
		},
		{
			value: question5,
			setValue: setQuestion5,
			error: question5Error,
			setError: setQuestion5Error,
		},
	];

	const [updateValues] = useMutation<updateServiceValueMutation, updateServiceValueMutationVariables>(
		UPDATE_SERVICE_VALUE_MUTATION,
	);

	const { loading, data } = useQuery<getServiceQuery, getServiceQueryVariables>(GET_SERVICES_QUERY, {
		variables: { companyGroupServiceId: selectedServiceId },
		fetchPolicy: 'no-cache',
	});

	useEffect(() => {
		if (data) {
			data.getService.forEach((val, index) => {
				if (val.value) {
					arrayOfQuestions[index].setValue(`${val.value}`);
				}
			});
		}
	}, [data]);

	const submit = () => {
		if (data) {
			submitValuesHandler({
				arrayOfQuestions,
				updateValues,
				setIsLoading,
				setSubmitModalIsOpen,
				data,
				t,
			});
		}
	};

	const errors = [question1Error, question2Error, question3Error, question4Error, question5Error];

	return (
		<>
			<Modal
				isOpen={submitModalIsOpen}
				className={styles.modalPopup}
				overlayClassName={styles.modalOverlayPopup}
				onRequestClose={() => setCurrentFrame('service')}
			>
				<div className={styles.popupWrapper}>
					<div className={styles.popupText}>
						<div className={styles.popupTextCommon}>{t('Dashboard.Answers for service')}</div>
						<div className={styles.popupTextBold}>{selectedGroupName}</div>
						<div className={styles.popupTextCommon}>{t('Dashboard.have been submitted to WeBench for')}</div>
						<div className={styles.popupTextBold}>{t('Dashboard.approval')}.</div>
						<div className={styles.popupTextCommon}>
							{t('Dashboard.Once approved you will be able to see result for')}
						</div>
						<div className={styles.popupTextBold}>{selectedGroupName}</div>
						<div className={styles.popupTextCommon}>{t('Dashboard.on your dashboard')}</div>
					</div>
					<div className={styles.buttonContainer}>
						<Button onClick={() => setCurrentFrame('service')}> {t('Dashboard.Ok')}</Button>
					</div>
				</div>
			</Modal>
			<div className={styles.Container}>
				<div
					className={styles.arrowContainer}
					onClick={() => {
						setCurrentFrame('service');
					}}
				>
					<img className={styles.arrow} src={arrowLeft} alt='' />
				</div>

				<div className={styles.title}>{t('Dashboard.Service')}</div>
				<div className={styles.serviceNameTitle}>{selectedGroupName}</div>

				<WithLoader isLoading={loading || isLoading}>
					<div className={styles.questionsContainer}>
						<div className={styles.subTitle}>{selectedServiceName}</div>
						<div className={styles.questionsContainerInput}>
							{data?.getService.map((value, index) => (
								<div key={value.id} className={styles.inputWrapper}>
									<Input
										isServiceQuestion
										key={value.id}
										label={value.label}
										placeholder={value.label}
										value={arrayOfQuestions[index].value}
										onChange={arrayOfQuestions[index].setValue}
										error={arrayOfQuestions[index].error}
									/>
								</div>
							))}
						</div>
					</div>
				</WithLoader>
				<div className={styles.buttonsContainer}>
					<Button
						type={'light'}
						onClick={() => {
							console.log('Save as draft :>> ', 'Save as draft');
						}}
					>
						{t('Dashboard.Save as draft')}
					</Button>
					<div>
						<ErrorMessage errors={errors} />
						<Button onClick={submit}>{t('Dashboard.Submit')}</Button>
					</div>
				</div>
			</div>
		</>
	);
};

export default QuestionsComponent;
