import { TFunction } from 'i18next';

import { GraphqlMutation } from 'library/common/types/graphql';
import {
	updateServiceValueMutation,
	updateServiceValueMutationVariables,
} from 'library/graphql/__generated__/updateServiceValueMutation';
import { getServiceQuery } from 'library/graphql/__generated__/getServiceQuery';

export const submitValuesHandler = async ({
	arrayOfQuestions,
	updateValues,
	setIsLoading,
	setSubmitModalIsOpen,
	data: serviceData,
	t,
}: {
	arrayOfQuestions: Array<{
		value: string;
		setValue: React.Dispatch<React.SetStateAction<string>>;
		error: string;
		setError: React.Dispatch<React.SetStateAction<string>>;
	}>;
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
	setSubmitModalIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
	updateValues: GraphqlMutation<updateServiceValueMutation, updateServiceValueMutationVariables>;
	data: getServiceQuery;
	t: TFunction;
}) => {
	const validationResult = validateSubmitValuesFields({
		arrayOfQuestions,
		t,
	});

	if (!validationResult) return;

	setIsLoading(true);
	try {
		const { data } = await updateValues({
			variables: {
				params: arrayOfQuestions.map((element, index) => ({
					id: serviceData?.getService[index].id,
					companyGroupServiceId: serviceData?.getService[index].companyGroupServiceId,
					value: +element.value.trim(),
					label: serviceData?.getService[index].label,
				})),
			},
		});

		if (data && data.updateServiceValue) {
			setSubmitModalIsOpen(true);
		}
	} catch ({ graphQLErrors }) {
		console.log(graphQLErrors);
	} finally {
		setIsLoading(false);
	}
};

export const validateSubmitValuesFields = ({
	arrayOfQuestions,
	t,
}: {
	arrayOfQuestions: Array<{
		value: string;
		setValue: React.Dispatch<React.SetStateAction<string>>;
		error: string;
		setError: React.Dispatch<React.SetStateAction<string>>;
	}>;
	t: TFunction;
}) => {
	let isSuccess = true;

	arrayOfQuestions.forEach(element => {
		if (element.value.trim().length === 0) {
			element.setError(t('Errors.Cannot be blank'));
			isSuccess = false;
		} else {
			element.setError('');
		}
	});

	return isSuccess;
};
