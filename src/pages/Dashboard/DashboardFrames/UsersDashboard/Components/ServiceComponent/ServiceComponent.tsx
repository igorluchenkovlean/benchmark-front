import React from 'react';
import { useTranslation } from 'react-i18next';
import cn from 'classnames';

import Button from 'library/common/components/Button/Button';

import styles from './serviceComponent.module.scss';

interface IServiceComponent {
	isService?: boolean;
	serviceGroupName: string;
	serviceQuantity: number;
	serviceDoneQuantity: number;
	companyGroupId: number;
	handleClick: (id: number, name: string) => void;
	isCompleted?: boolean;
}

const ServiceComponent = ({
	isService,
	serviceGroupName,
	serviceQuantity,
	serviceDoneQuantity,
	companyGroupId,
	handleClick,
	isCompleted,
}: IServiceComponent) => {
	const { t } = useTranslation();

	return (
		<div className={cn(styles.serviceContainer, { [styles.serviceContainerSubmited]: isCompleted })}>
			<div className={cn(styles.title, { [styles.titleSubmited]: isCompleted })}>{serviceGroupName}</div>
			<div className={styles.subTitle}>
				{serviceDoneQuantity !== 0 ? (
					<span>{`${serviceDoneQuantity}/${serviceQuantity} `}</span>
				) : (
					<span>{`${serviceQuantity} `}</span>
				)}

				{isService ? <span>{t('Dashboard.Questions')}</span> : <span>{t('Dashboard.Services')}</span>}
			</div>
			<div className={styles.button}>
				<Button
					onClick={() => {
						handleClick(companyGroupId, serviceGroupName);
					}}
				>
					{isCompleted ? t('Dashboard.Edit') : t('Dashboard.Start')}
				</Button>
			</div>
		</div>
	);
};

export default ServiceComponent;
