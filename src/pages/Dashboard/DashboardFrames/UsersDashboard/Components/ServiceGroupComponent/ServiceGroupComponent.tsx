import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/react-hooks';

import { GET_COMPANY_GROUPS_SERVICES_QUERY } from 'library/graphql/companies';
import {
	getCompanyGroupServicesQuery,
	getCompanyGroupServicesQueryVariables,
	getCompanyGroupServicesQuery_getCompanyGroupServices,
} from 'library/graphql/__generated__/getCompanyGroupServicesQuery';
import arrowLeft from 'resources/icons/arrowLeft.svg';
import WithLoader from 'library/common/components/WithLoader';
import ServiceComponent from '../ServiceComponent';
import { DashboardFrame } from '../../UsersDashboard';

import styles from './serviceGroupComponent.module.scss';

interface IServiceGroups {
	companyGroupServices: getCompanyGroupServicesQuery_getCompanyGroupServices[];
	setCompanyGroupServices: React.Dispatch<React.SetStateAction<getCompanyGroupServicesQuery_getCompanyGroupServices[]>>;
	setSelectedServiceName: React.Dispatch<React.SetStateAction<string>>;
	selectedGroupName: string;
	selectedGroupId: number;
	setCurrentFrame: React.Dispatch<React.SetStateAction<DashboardFrame>>;
	setSelectedServiceId: React.Dispatch<React.SetStateAction<number>>;
}

const ServiceGroupComponent = ({
	companyGroupServices,
	setCompanyGroupServices,
	setSelectedServiceName,
	selectedGroupName,
	selectedGroupId,
	setCurrentFrame,
	setSelectedServiceId,
}: IServiceGroups) => {
	const { t } = useTranslation();

	const { loading, data } = useQuery<getCompanyGroupServicesQuery, getCompanyGroupServicesQueryVariables>(
		GET_COMPANY_GROUPS_SERVICES_QUERY,
		{
			variables: { groupId: selectedGroupId },
			fetchPolicy: 'cache-and-network',
		},
	);
	useEffect(() => {
		if (data) {
			setCompanyGroupServices(data.getCompanyGroupServices);
		}
	}, [data, selectedGroupName]);

	const handleClick = (id: number, name: string) => {
		setCurrentFrame('questions');
		setSelectedServiceId(id);
		setSelectedServiceName(name);
	};

	return (
		<div className={styles.Container}>
			<div
				className={styles.arrowContainer}
				onClick={() => {
					setCurrentFrame('serviceGroups');
				}}
			>
				<img className={styles.arrow} src={arrowLeft} alt='' />
			</div>

			<div className={styles.title}>{t('Dashboard.Service')}</div>
			<div className={styles.serviceNameTitle}>{selectedGroupName}</div>
			<div className={styles.subTitle}>
				{t('Dashboard.Kindly select service for which you would like to add answers for benchmarking')}
			</div>
			<WithLoader isLoading={loading}>
				<div className={styles.servicesContainer}>
					{companyGroupServices.map(value => (
						<ServiceComponent
							key={value.companyGroupServiceId}
							isService
							serviceGroupName={value.serviceName}
							serviceQuantity={5}
							serviceDoneQuantity={0}
							companyGroupId={value.companyGroupServiceId}
							handleClick={handleClick}
							isCompleted={value.isSubmit}
						/>
					))}
				</div>
			</WithLoader>
		</div>
	);
};

export default ServiceGroupComponent;
