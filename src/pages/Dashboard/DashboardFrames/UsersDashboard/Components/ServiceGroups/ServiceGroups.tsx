import React from 'react';
import { useTranslation } from 'react-i18next';

import { getCompanyGroupsQuery_getCompanyGroups } from 'library/graphql/__generated__/getCompanyGroupsQuery';
import WithLoader from 'library/common/components/WithLoader';
import ServiceComponent from '../ServiceComponent';
import { DashboardFrame } from '../../UsersDashboard';

import styles from './serviceGroups.module.scss';

interface IServiceGroups {
	companyGroupsDataLoading: boolean;
	companyGroups: getCompanyGroupsQuery_getCompanyGroups[];
	setSelectedGroupName: React.Dispatch<React.SetStateAction<string>>;
	setCurrentFrame: React.Dispatch<React.SetStateAction<DashboardFrame>>;
	setSelectedGroupId: React.Dispatch<React.SetStateAction<number>>;
}

const ServiceGroups = ({
	companyGroupsDataLoading,
	companyGroups,
	setSelectedGroupName,
	setCurrentFrame,
	setSelectedGroupId,
}: IServiceGroups) => {
	const { t } = useTranslation();

	const handleClick = (id: number, name: string) => {
		setSelectedGroupName(name);
		setCurrentFrame('service');
		setSelectedGroupId(id);
	};

	return (
		<div className={styles.Container}>
			<div className={styles.title}>{t('Dashboard.Service Group')}</div>
			<div className={styles.subTitle}>
				{t('Dashboard.Kindly select service group for which you would like to add answers for benchmarking')}
			</div>
			<WithLoader isLoading={companyGroupsDataLoading}>
				<div className={styles.servicesContainer}>
					{companyGroups.map(value => (
						<ServiceComponent
							key={value.companyGroupId}
							serviceGroupName={value.serviceGroupName}
							serviceQuantity={value.servicesCount}
							serviceDoneQuantity={0}
							companyGroupId={value.companyGroupId}
							handleClick={handleClick}
						/>
					))}
				</div>
			</WithLoader>
		</div>
	);
};

export default ServiceGroups;
