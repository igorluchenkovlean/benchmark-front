import React, { useState, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation, useQuery } from '@apollo/react-hooks';

import { GET_COMPANY_GROUPS_QUERY, UPLOAD_FILE_MUTATION } from 'library/graphql/companies';
import {
	getCompanyGroupsQuery,
	getCompanyGroupsQuery_getCompanyGroups,
} from 'library/graphql/__generated__/getCompanyGroupsQuery';
import { uploadFileMutation, uploadFileMutationVariables } from 'library/graphql/__generated__/uploadFileMutation';
import { getCompanyGroupServicesQuery_getCompanyGroupServices } from 'library/graphql/__generated__/getCompanyGroupServicesQuery';
import Button from 'library/common/components/Button/Button';
import WithLoader from 'library/common/components/WithLoader';
import upload from 'resources/icons/upload.png';
import download from 'resources/icons/download.png';
import ServiceGroups from './Components/ServiceGroups';
import ServiceGroupComponent from './Components/ServiceGroupComponent';
import QuestionsComponent from './Components/QuestionsComponent';

import styles from './usersDashboard.module.scss';

export type DashboardFrame = 'serviceGroups' | 'questions' | 'service';

const UsersDashboard = () => {
	const { t } = useTranslation();
	const buttonRef = useRef<HTMLInputElement | null>(null);
	const [companyGroups, setCompanyGroups] = useState<getCompanyGroupsQuery_getCompanyGroups[]>([]);
	const [companyGroupServices, setCompanyGroupServices] = useState<
		getCompanyGroupServicesQuery_getCompanyGroupServices[]
	>([]);
	const [selectedGroupId, setSelectedGroupId] = useState(0);
	const [selectedGroupName, setSelectedGroupName] = useState('');
	const [selectedServiceId, setSelectedServiceId] = useState(0);
	const [selectedServiceName, setSelectedServiceName] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [currentFrame, setCurrentFrame] = useState<DashboardFrame>('serviceGroups');

	const [uploadFileMutationFunction] = useMutation<uploadFileMutation, uploadFileMutationVariables>(
		UPLOAD_FILE_MUTATION,
	);
	const { loading: companyGroupsDataLoading, data: CompanyGroupsData } = useQuery<getCompanyGroupsQuery>(
		GET_COMPANY_GROUPS_QUERY,
	);

	useEffect(() => {
		if (CompanyGroupsData) {
			setCompanyGroups(CompanyGroupsData.getCompanyGroups);
		}
	}, [CompanyGroupsData]);

	const getBase64 = (fileUploaded: File) =>
		new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(fileUploaded);
			reader.onload = () => resolve(reader.result);
			reader.onerror = error => reject(error);
		});

	const handleClick = () => {
		buttonRef.current?.click();
	};

	const onChangeFile = async (event: React.ChangeEvent<HTMLInputElement>) => {
		event.preventDefault();
		const fileOne = event.target.files && event.target.files[0];

		setIsLoading(true);
		try {
			if (fileOne) {
				const fileBase64 = await getBase64(fileOne);

				const { data } = await uploadFileMutationFunction({
					variables: {
						file: fileBase64 as any,
					},
				});
				if (data?.uploadFile) {
					setCompanyGroups(data.uploadFile);
				}
			}
		} catch ({ graphQLErrors }) {
			console.log(graphQLErrors);
		} finally {
			setIsLoading(false);
		}
	};

	return (
		<div className={styles.dashboardContainer}>
			<WithLoader isLoading={isLoading}>
				<div>
					{currentFrame === 'serviceGroups' && (
						<ServiceGroups
							companyGroupsDataLoading={companyGroupsDataLoading}
							companyGroups={companyGroups}
							setSelectedGroupName={setSelectedGroupName}
							setCurrentFrame={setCurrentFrame}
							setSelectedGroupId={setSelectedGroupId}
						/>
					)}
					{currentFrame === 'service' && (
						<ServiceGroupComponent
							companyGroupServices={companyGroupServices}
							setCompanyGroupServices={setCompanyGroupServices}
							setSelectedServiceName={setSelectedServiceName}
							selectedGroupName={selectedGroupName}
							selectedGroupId={selectedGroupId}
							setCurrentFrame={setCurrentFrame}
							setSelectedServiceId={setSelectedServiceId}
						/>
					)}

					{currentFrame === 'questions' && (
						<QuestionsComponent
							selectedGroupName={selectedGroupName}
							selectedServiceName={selectedServiceName}
							selectedServiceId={selectedServiceId}
							setCurrentFrame={setCurrentFrame}
						/>
					)}
				</div>
			</WithLoader>
			{currentFrame !== 'questions' && (
				<div>
					<div className={styles.text}>
						{t('Dashboard.You can skip answering any service and you won’t be able to view result for that service')}
					</div>
					<div className={styles.buttonsContainer}>
						<Button
							type='light'
							onClick={() => {
								console.log('Download Excel :>> ', 'Download Excel');
							}}
						>
							<div className={styles.buttonInner}>
								<img className={styles.buttonLogo} src={download} alt='' />
								<span>{t('Dashboard.Download Excel')}</span>
							</div>
						</Button>
						<Button onClick={handleClick}>
							<div className={styles.buttonInner}>
								<img className={styles.buttonLogo} src={upload} alt='' />
								<span>{t('Dashboard.Upload Excel')}</span>
								<input type='file' ref={buttonRef} style={{ display: 'none' }} onChange={onChangeFile} />
							</div>
						</Button>
					</div>
				</div>
			)}
		</div>
	);
};

export default UsersDashboard;
