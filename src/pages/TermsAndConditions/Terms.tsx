import React from 'react';
import { useTranslation } from 'react-i18next';

import { termsText } from './utils/termsText';

import styles from './terms.module.scss';
import Button from 'library/common/components/Button/Button';

interface ITerms {
	setModalIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
	setTermsChecked: React.Dispatch<React.SetStateAction<boolean>>;
}

const Terms = ({ setModalIsOpen, setTermsChecked }: ITerms) => {
	const { t } = useTranslation();

	const agreeHandler = () => {
		setModalIsOpen(false);
		setTermsChecked(true);
	};

	return (
		<div className={styles.container}>
			<div className={styles.textHeader}>{t("Terms.General Business Condition 'WeBench'")}</div>
			{termsText.map((text, index) => (
				<div key={index + 100} className={styles.text}>
					{`${!!text.sign ? text.sign : ''} ${t(text.text)}`}
				</div>
			))}
			<div className={styles.buttonContainer}>
				<Button type={'red'} onClick={() => setModalIsOpen(false)}>
					{t('Terms.Cancel')}
				</Button>
				<Button onClick={agreeHandler}>{t('Terms.I Agree')}</Button>
			</div>
		</div>
	);
};

export default Terms;
