export const termsText = [
	{
		sign: 'I.',
		text: 'Terms.Scope of these terms and conditions and general provisions',
	},
	{
		sign: '§ 1',
		text: 'Terms.Scope for the business relationship',
	},
	{
		sign: '§ 2',
		text: 'Terms.Changes to Terms and Conditions',
	},
	{
		sign: '',
		text: 'Terms.Changes to terms and conditions shall be communicated',
	},
	{
		sign: '§ 3',
		text: 'Terms.Conclusion of the contract',
	},
	{
		sign: '',
		text: 'Terms.Unless otherwise stated in the offer',
	},
	{
		sign: '§ 4',
		text: 'Terms.Processing of personal data for the performance of the business relationship',
	},
	{
		sign: '',
		text: 'Terms.JPN processes the customers personal data',
	},
	{
		sign: '§ 5',
		text: 'Terms.Application of German law',
	},
	{
		sign: '',
		text: 'Terms.The business relationship between the customer and JPN',
	},
	{
		sign: '§ 6',
		text: 'Terms.Place of performance and place of jurisdiction',
	},
	{
		sign: '',
		text: 'Terms.Place of performance is Hannover',
	},
	{
		sign: '§ 7',
		text: 'Terms.Text Form',
	},
	{
		sign: '',
		text: 'Terms.Changes and additions to the contract require the text form to be effective',
	},
	{
		sign: 'II.',
		text: 'Terms.The nature of the services and rights of use',
	},
	{
		sign: '§ 8',
		text: 'Terms.Content and scope of the services offered',
	},
	{
		sign: '',
		text: 'Terms.(1) JPNs services may include',
	},
	{
		sign: '',
		text: 'Terms.(2) The data for their WeBench shall be used by JPN through submissions',
	},
	{
		sign: '',
		text: 'Terms.(3) Unless expressly stated otherwise in the service contract with the customer',
	},
	{
		sign: '§ 9',
		text: 'Terms.Characteristics and significance of the services',
	},
	{
		sign: '',
		text: 'Terms.Information in documentation, test and promotional materials',
	},
	{
		sign: '§ 10',
		text: 'Terms.General Terms and Conditions of Use',
	},
	{
		sign: '',
		text: 'Terms.Insofar as there is nothing else to be found out from any product-specific',
	},
	{
		sign: '',
		text: 'Terms.1 The customer shall be granted a simple',
	},
	{
		sign: '',
		text: 'Terms.2 The exercise of the rights of use is only permitted',
	},
	{
		sign: '',
		text: 'Terms.3 In the case of permanent obligations',
	},
	{
		sign: '§ 11',
		text: 'Terms.Copyright, Trademark and Trademark Protection',
	},
	{
		sign: '',
		text: 'Terms.(1) The databases available via JPN are a database work',
	},
	{
		sign: '',
		text: 'Terms.(2) All intellectual property rights',
	},
	{
		sign: '',
		text: 'Terms.(3) Trademarks, company logos, copyright notices and all other',
	},
	{
		sign: '§ 12',
		text: 'Terms.Terms of Use and Participation in the Transfer of Personal Data',
	},
	{
		sign: '',
		text: 'Terms.(1) Personal data within the meaning of Article',
	},
	{
		sign: '',
		text: 'Terms.(2) Insofar as the transmission of personal data within the meaning',
	},
	{
		sign: '§ 13',
		text: 'Terms.Access to online services, availability',
	},
	{
		sign: '',
		text: 'Terms.(1) JPN provides certain services for use via the Internet',
	},
	{
		sign: '',
		text: 'Terms.(2) JPN online services are generally designed',
	},
	{
		sign: '',
		text: 'Terms.(3) The customer reaches the online services via a registration',
	},
	{
		sign: '',
		text: 'Terms.(4) JPN reserves the right to refuse access to online services',
	},
	{
		sign: '',
		text: 'Terms.(5) JPN may adapt its online services and services to current requirements',
	},
	{
		sign: '§ 14',
		text: 'Terms.Confidentiality',
	},
	{
		sign: '',
		text: 'Terms.Without prejudice to the data protection obligations',
	},
	{
		sign: 'III.',
		text: 'Terms.Settlement and payment terms',
	},
	{
		sign: '§ 15',
		text: 'Terms.Prices',
	},
	{
		sign: '',
		text: 'Terms.Unless otherwise stated',
	},
	{
		sign: '§ 16',
		text: 'Terms.Price changes',
	},
	{
		sign: '',
		text: 'Terms.(1) WITHin the framework of an ongoing permanent debt relationship',
	},
	{
		sign: '',
		text: 'Terms.(2) If price changes for a service amount to more than five percent within a calendar year',
	},
	{
		sign: '§ 17',
		text: 'Terms.Settlement for quantity-based orders with prior potential analysis',
	},
	{
		sign: '',
		text: 'Terms.If the customer wants to obtain data sets on the basis of certain selection',
	},
	{
		sign: '§ 18',
		text: 'Terms.Due date, audit of settlement',
	},
	{
		sign: '',
		text: 'Terms.All payments are due immediately and without deduction after receipt of the invoice',
	},
	{
		sign: '§ 19',
		text: 'Terms.Offsetting or retention rights of the customer can only',
	},
	{
		sign: '§ 20',
		text: 'Terms.Retention of title',
	},
	{
		sign: '',
		text: 'Terms.The transfer of agreed rights of use as well as the transfer',
	},
	{
		sign: '§ 21',
		text: 'Terms.Infringements',
	},
	{
		sign: '',
		text: 'Terms.If the customer violates the obligations under his or her obligations considerably',
	},
	{
		sign: '§ 22',
		text: 'Terms.Claims for Defects',
	},
	{
		sign: '',
		text: 'Terms.(1) JPN guarantees the agreed quality of the services received and that',
	},
	{
		sign: '',
		text: 'Terms.(2) If there is a warranty reason, the customer must first set',
	},
	{
		sign: '',
		text: 'Terms.(3) Rights resulting from the defectiveness of the services',
	},
	{
		sign: '',
		text: 'Terms.(4) Claims against JPN due to functional impairments or performance disruptions',
	},
	{
		sign: '§ 23',
		text: 'Terms.Liability for damage to the customer',
	},
	{
		sign: '',
		text: 'Terms.(1) JPN shall be liable in accordance with the statutory provisions',
	},
	{
		sign: '',
		text: 'Terms.(2) In the event of slight negligence, JPN shall only be liable for damages',
	},
	{
		sign: '',
		text: 'Terms.(3) The limitations of liability do not apply in cases of mandatory',
	},
	{
		sign: '§ 24',
		text: 'Terms.Indemary liability',
	},
	{
		sign: '',
		text: 'Terms.The customer indemnifies JPN from all claims of third parties',
	},
	{
		sign: '§ 25',
		text: 'Terms.Limitation period',
	},
	{
		sign: '',
		text: 'Terms.Warranty and compensation claims shall be forfeited if they are not asserted',
	},
];
